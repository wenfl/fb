package com.wuhuacloud.fb.mapper;

import java.util.List;
import java.util.Map;

import com.wuhuacloud.fb.domain.Canteen;
import com.wuhuacloud.fb.domain.CanteenMenu;
import org.apache.ibatis.annotations.Param;

/**
 * 食堂Mapper接口
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public interface CanteenMapper 
{
    /**
     * 查询食堂
     * 
     * @param id 食堂ID
     * @return 食堂
     */
    public Canteen selectCanteenById(Long id);

    /**
     * 查询食堂列表
     * 
     * @param canteen 食堂
     * @return 食堂集合
     */
    public List<Canteen> selectCanteenList(Canteen canteen);

    /**
     * 新增食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    public int insertCanteen(Canteen canteen);

    /**
     * 修改食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    public int updateCanteen(Canteen canteen);

    /**
     * 删除食堂
     * 
     * @param id 食堂ID
     * @return 结果
     */
    public int deleteCanteenById(Long id);

    /**
     * 批量删除食堂
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCanteenByIds(Long[] ids);

    /**
     * 查询食堂与菜单关联列表
     *
     * @param canteenMenu 食堂与菜单关联
     * @return 食堂与菜单关联集合
     */
    public List<CanteenMenu> getDayCanteenMenu(CanteenMenu canteenMenu);

    /**
     * 新增食堂与菜单关联
     *
     * @param canteenMenu 食堂与菜单关联
     * @return 结果
     */
    public int editDayCanteenMenu(CanteenMenu canteenMenu);

    void delCanteenMenut(Map dataMap);

    public List<Canteen>  selectCanteenDept(Long deptId);

    void delCanteenDeop(Long deptId);

    void saveCanteenDeop(@Param("deptId")Long deptId, @Param("canteenId")String canteenId);

    /**
     * 查询食堂与菜单关联列表
     *
     * @param canteenMenu 食堂与菜单关联
     * @return 食堂与菜单关联集合
     */
    public List<CanteenMenu> selectCanteenMenuList(CanteenMenu canteenMenu);

}
