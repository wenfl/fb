package com.wuhuacloud.fb.mapper;

import java.util.List;
import java.util.Map;

import com.wuhuacloud.fb.domain.TradeInfo;

/**
 * 交易记录Mapper接口
 * 
 * @author wenfl
 * @date 2021-01-04
 */
public interface TradeInfoMapper 
{
    /**
     * 查询交易记录
     * 
     * @param tradeId 交易记录ID
     * @return 交易记录
     */
    public TradeInfo selectTradeInfoById(Long tradeId);

    /**
     * 查询交易记录列表
     * 
     * @param tradeInfo 交易记录
     * @return 交易记录集合
     */
    public List<TradeInfo> selectTradeInfoList(TradeInfo tradeInfo);

    public List<TradeInfo> selectTradeInfoListDiy(Map query);

    /**
     * 查询 消费、充值等记录信息，统计 根据年月和用户id
     * @param query
     * @return
     */
    public Map<String,String> selectTradeInfoAll(Map query);

    /**
     * 新增交易记录
     * 
     * @param tradeInfo 交易记录
     * @return 结果
     */
    public int insertTradeInfo(TradeInfo tradeInfo);

    /**
     * 修改交易记录
     * 
     * @param tradeInfo 交易记录
     * @return 结果
     */
    public int updateTradeInfo(TradeInfo tradeInfo);

    /**
     * 删除交易记录
     * 
     * @param tradeId 交易记录ID
     * @return 结果
     */
    public int deleteTradeInfoById(Long tradeId);

    /**
     * 批量删除交易记录
     * 
     * @param tradeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTradeInfoByIds(Long[] tradeIds);
}
