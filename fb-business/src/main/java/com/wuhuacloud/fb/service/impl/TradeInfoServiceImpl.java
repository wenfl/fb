package com.wuhuacloud.fb.service.impl;

import java.util.List;
import java.util.Map;

import com.wuhuacloud.common.utils.DateUtils;
import com.wuhuacloud.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.TradeInfoMapper;
import com.wuhuacloud.fb.domain.TradeInfo;
import com.wuhuacloud.fb.service.ITradeInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 交易记录Service业务层处理
 * 
T * @author wenfl
 * @date 2021-01-04
 */
@Service
public class TradeInfoServiceImpl implements ITradeInfoService 
{
    @Autowired
    private TradeInfoMapper tradeInfoMapper;

    /**
     * 查询交易记录
     * 
     * @param tradeId 交易记录ID
     * @return 交易记录
     */
    @Override
    public TradeInfo selectTradeInfoById(Long tradeId)
    {
        return tradeInfoMapper.selectTradeInfoById(tradeId);
    }

    /**
     * 查询交易记录列表
     * 
     * @param tradeInfo 交易记录
     * @return 交易记录
     */
    @Override
    public List<TradeInfo> selectTradeInfoList(TradeInfo tradeInfo)
    {
        return tradeInfoMapper.selectTradeInfoList(tradeInfo);
    }

    /**
     * 查询交易记录列表
     *
     * @param tradeInfo 交易记录
     * @return 交易记录
     */
    @Override
    public List<TradeInfo> selectTradeInfoListDiy(Map query)
    {
        return tradeInfoMapper.selectTradeInfoListDiy(query);
    }

    @Override
    public Map<String, String> selectTradeInfoAll(Map query) {
        return tradeInfoMapper.selectTradeInfoAll(query);
    }

    /**
     * 新增交易记录
     * 
     * @param tradeInfo 交易记录
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertTradeInfo(TradeInfo tradeInfo)
    {
        //交易ID-UUID
        tradeInfo.setTradeId(IdUtils.simpleUUID());
        //交易时间T
        tradeInfo.setTradeTime(DateUtils.getNowDate());
        tradeInfo.setCreateTime(DateUtils.getNowDate());
        return tradeInfoMapper.insertTradeInfo(tradeInfo);
    }

    /**
     * 修改交易记录
     * 
     * @param tradeInfo 交易记录
     * @return 结果
     */
    @Override
    public int updateTradeInfo(TradeInfo tradeInfo)
    {
        tradeInfo.setUpdateTime(DateUtils.getNowDate());
        return tradeInfoMapper.updateTradeInfo(tradeInfo);
    }

    /**
     * 批量删除交易记录
     * 
     * @param tradeIds 需要删除的交易记录ID
     * @return 结果
     */
    @Override
    public int deleteTradeInfoByIds(Long[] tradeIds)
    {
        return tradeInfoMapper.deleteTradeInfoByIds(tradeIds);
    }

    /**
     * 删除交易记录信息
     * 
     * @param tradeId 交易记录ID
     * @return 结果
     */
    @Override
    public int deleteTradeInfoById(Long tradeId)
    {
        return tradeInfoMapper.deleteTradeInfoById(tradeId);
    }
}
