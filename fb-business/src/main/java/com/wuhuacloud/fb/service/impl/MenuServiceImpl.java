package com.wuhuacloud.fb.service.impl;

import java.util.List;
import com.wuhuacloud.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.MenuMapper;
import com.wuhuacloud.fb.domain.Menu;
import com.wuhuacloud.fb.service.IMenuService;

/**
 * 食堂菜单Service业务层处理
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@Service
public class MenuServiceImpl implements IMenuService 
{
    @Autowired
    private MenuMapper menuMapper;

    /**
     * 查询食堂菜单
     * 
     * @param menuId 食堂菜单ID
     * @return 食堂菜单
     */
    @Override
    public Menu selectMenuById(Long menuId)
    {
        return menuMapper.selectMenuById(menuId);
    }

    /**
     * 查询食堂菜单列表
     * 
     * @param menu 食堂菜单
     * @return 食堂菜单
     */
    @Override
    public List<Menu> selectMenuList(Menu menu)
    {
        return menuMapper.selectMenuList(menu);
    }

    /**
     * 新增食堂菜单
     * 
     * @param menu 食堂菜单
     * @return 结果
     */
    @Override
    public int insertMenu(Menu menu)
    {
        menu.setCreateTime(DateUtils.getNowDate());
        return menuMapper.insertMenu(menu);
    }

    /**
     * 修改食堂菜单
     * 
     * @param menu 食堂菜单
     * @return 结果
     */
    @Override
    public int updateMenu(Menu menu)
    {
        menu.setUpdateTime(DateUtils.getNowDate());
        return menuMapper.updateMenu(menu);
    }

    /**
     * 批量删除食堂菜单
     * 
     * @param menuIds 需要删除的食堂菜单ID
     * @return 结果
     */
    @Override
    public int deleteMenuByIds(Long[] menuIds)
    {
        return menuMapper.deleteMenuByIds(menuIds);
    }

    /**
     * 删除食堂菜单信息
     * 
     * @param menuId 食堂菜单ID
     * @return 结果
     */
    @Override
    public int deleteMenuById(Long menuId)
    {
        return menuMapper.deleteMenuById(menuId);
    }
}
