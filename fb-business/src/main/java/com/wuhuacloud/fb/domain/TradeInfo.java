package com.wuhuacloud.fb.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 交易记录对象 fb_trade_info
 *
 * @author wenfl
 * @date 2021-01-09
 */
public class TradeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 交易时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交易时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date tradeTime;

    /** 交易IP */
    @Excel(name = "交易IP")
    private String tradeIp;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 交易ID */
    @Excel(name = "交易ID")
    private String tradeId;

    /** 交易类型 */
    @Excel(name = "交易类型")
    private String tradeType;

    /** 交易金额 */
    @Excel(name = "交易金额")
    private String payment;

    /** 食堂编号 */
    @Excel(name = "食堂编号")
    private Long canteenId;

    public Date getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(Date tradeTime) {
        this.tradeTime = tradeTime;
    }

    public void setTradeIp(String tradeIp)
    {
        this.tradeIp = tradeIp;
    }

    public String getTradeIp()
    {
        return tradeIp;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTradeId(String tradeId)
    {
        this.tradeId = tradeId;
    }

    public String getTradeId()
    {
        return tradeId;
    }
    public void setTradeType(String tradeType)
    {
        this.tradeType = tradeType;
    }

    public String getTradeType()
    {
        return tradeType;
    }
    public void setPayment(String payment)
    {
        this.payment = payment;
    }

    public String getPayment()
    {
        return payment;
    }
    public void setCanteenId(Long canteenId)
    {
        this.canteenId = canteenId;
    }

    public Long getCanteenId()
    {
        return canteenId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("tradeTime", getTradeTime())
                .append("tradeIp", getTradeIp())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("userId", getUserId())
                .append("tradeId", getTradeId())
                .append("tradeType", getTradeType())
                .append("payment", getPayment())
                .append("canteenId", getCanteenId())
                .toString();
    }
}