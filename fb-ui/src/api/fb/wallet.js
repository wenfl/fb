import request from '@/utils/request'

// 查询余额列表
export function listWallet(query) {
  return request({
    url: '/fb/wallet/list',
    method: 'get',
    params: query
  })
}

// 查询余额详细
export function getWallet(userId) {
  return request({
    url: '/fb/wallet/' + userId,
    method: 'get'
  })
}

// 新增余额
export function addWallet(data) {
  return request({
    url: '/fb/wallet',
    method: 'post',
    data: data
  })
}

// 修改余额
export function updateWallet(data) {
  return request({
    url: '/fb/wallet',
    method: 'put',
    data: data
  })
}

// 删除余额
export function delWallet(userId) {
  return request({
    url: '/fb/wallet/' + userId,
    method: 'delete'
  })
}

// 导出余额
export function exportWallet(query) {
  return request({
    url: '/fb/wallet/export',
    method: 'get',
    params: query
  })
}

//充值
export function recharge(data){
  return request({
    url: '/fb/wallet/recharge',
    method: 'get',
    params: data
  })
}