import request from '@/utils/request'

// 查询交易记录列表
export function listInfo(query) {
  return request({
    url: '/fb/info/list',
    method: 'get',
    params: query
  })
}

// 查询交易记录详细
export function getInfo(tradeId) {
  return request({
    url: '/fb/info/' + tradeId,
    method: 'get'
  })
}

// 新增交易记录
export function addInfo(data) {
  return request({
    url: '/fb/info',
    method: 'post',
    data: data
  })
}

// 修改交易记录
export function updateInfo(data) {
  return request({
    url: '/fb/info',
    method: 'put',
    data: data
  })
}

// 删除交易记录
export function delInfo(tradeId) {
  return request({
    url: '/fb/info/' + tradeId,
    method: 'delete'
  })
}

// 导出交易记录
export function exportInfo(query) {
  return request({
    url: '/fb/info/export',
    method: 'get',
    params: query
  })
}