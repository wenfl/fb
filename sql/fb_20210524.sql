/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云-永信120
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 120.78.136.131:3306
 Source Schema         : wtest

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 24/05/2021 15:36:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fb_canteen
-- ----------------------------
DROP TABLE IF EXISTS `fb_canteen`;
CREATE TABLE `fb_canteen`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '食堂编号',
  `canteen_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '食堂名称',
  `canteen_tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '食堂电话',
  `canteen_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '食堂地址',
  `canteen_principal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '食堂负责人',
  `canteen_longitude` decimal(10, 7) DEFAULT NULL COMMENT '食堂经度',
  `canteen_latitude` decimal(10, 7) DEFAULT NULL COMMENT '食堂纬度',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `canteen_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `canteen_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺logo',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '食堂表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_canteen
-- ----------------------------
INSERT INTO `fb_canteen` VALUES (4, '云计算公司', '13800138000', '广州南沙', '温生', NULL, NULL, '', '2021-01-12 11:03:57', '', NULL, NULL, 1, '1', '0', '0', NULL);
INSERT INTO `fb_canteen` VALUES (5, '知产公司', '13800138000', '广州', NULL, NULL, NULL, '', '2021-01-12 11:04:30', '', NULL, NULL, 3, '1', '0', '0', NULL);
INSERT INTO `fb_canteen` VALUES (3, '集团食堂', '13800138000', '中国南沙', NULL, NULL, NULL, '', '2021-01-12 11:02:18', '', NULL, NULL, 0, '1', '0', '0', NULL);

-- ----------------------------
-- Table structure for fb_canteen_dept
-- ----------------------------
DROP TABLE IF EXISTS `fb_canteen_dept`;
CREATE TABLE `fb_canteen_dept`  (
  `dept_id` bigint(20) NOT NULL COMMENT '部门id',
  `canteen_id` bigint(20) NOT NULL COMMENT '食堂编号'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '食堂与部门关联表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of fb_canteen_dept
-- ----------------------------
INSERT INTO `fb_canteen_dept` VALUES (103, 3);
INSERT INTO `fb_canteen_dept` VALUES (103, 5);

-- ----------------------------
-- Table structure for fb_canteen_menu
-- ----------------------------
DROP TABLE IF EXISTS `fb_canteen_menu`;
CREATE TABLE `fb_canteen_menu`  (
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  `canteen_id` bigint(20) NOT NULL COMMENT '食堂编号',
  `meal_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '餐类型（早中晚下午茶）',
  `meal_date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日期',
  `menu_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单类型（荤菜、素菜、组合）',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `menu_price` decimal(5, 2) DEFAULT NULL COMMENT '菜单价格',
  `canteen_price` decimal(5, 2) DEFAULT NULL COMMENT '食堂价格',
  `menu_img` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图片'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '食堂与菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_canteen_menu
-- ----------------------------
INSERT INTO `fb_canteen_menu` VALUES (4, 1, 'zao', '2020-12-29', '测试1', 'Z', 0, 15.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 1, 'zao', '2020-12-29', '青菜', 'S', 0, 15.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 1, 'zao', '2020-12-29', '红烧肉', 'H', 1, 15.00, 19.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 1, 'zhone', '2020-12-29', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 1, 'zhone', '2020-12-29', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 1, 'zhone', '2020-12-29', '青菜', 'S', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 1, 'zhone', '2020-12-29', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 1, 'xia', '2020-12-29', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 1, 'xia', '2020-12-29', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 1, 'xia', '2020-12-29', '青菜', 'S', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 1, 'xia', '2020-12-29', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 4, 'zao', '2021-01-21', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 4, 'zhone', '2021-01-21', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 4, 'zhone', '2021-01-20', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 4, 'xia', '2021-01-20', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 4, 'xia', '2021-01-21', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 3, 'zao', '2021-01-23', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'zao', '2021-01-23', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 3, 'zao', '2021-01-23', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 3, 'zao', '2021-01-23', '青菜', 'S', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'zao', '2021-01-23', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 3, 'zao', '2021-01-23', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 3, 'zao', '2021-01-23', '青菜', 'S', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 3, 'zao', '2021-01-23', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 3, 'zhone', '2021-01-23', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'zhone', '2021-01-23', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'xia', '2021-01-23', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 3, 'xia', '2021-01-23', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (3, 3, 'xia', '2021-01-23', '青菜', 'S', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg');
INSERT INTO `fb_canteen_menu` VALUES (4, 3, 'xia', '2021-01-23', '测试1', 'Z', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'wan', '2021-01-23', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (2, 3, 'wan', '2021-01-23', '红烧排骨', 'H', 0, 20.00, 20.00, 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg');
INSERT INTO `fb_canteen_menu` VALUES (5, 3, 'zao', '2021-01-23', '大刀肉', 'H', 0, 20.00, 20.00, 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif');
INSERT INTO `fb_canteen_menu` VALUES (1, 3, 'zao', '2021-02-01', '红烧肉', 'H', 1, 15.00, 15.00, 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg');
INSERT INTO `fb_canteen_menu` VALUES (5, 3, 'zhone', '2021-02-01', '大刀肉', 'H', 0, 20.00, 20.00, 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif');

-- ----------------------------
-- Table structure for fb_canteen_time
-- ----------------------------
DROP TABLE IF EXISTS `fb_canteen_time`;
CREATE TABLE `fb_canteen_time`  (
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `canteen_id` bigint(20) NOT NULL COMMENT '食堂编号',
  `meal_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '餐类型（早中晚下午茶）',
  `start_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开始时间',
  `stop_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结束时间',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '食堂营业时间' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_canteen_time
-- ----------------------------
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:44', NULL, 2, 'wan', '18:30', '19:30', 3);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:44', NULL, 2, 'zhone', '11:45', '12:15', 1);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:44', NULL, 2, 'xia', '15:30', '16:00', 2);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:44', NULL, 2, 'zao', '07:00', '07:30', 0);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:00', NULL, 1, 'wan', '18:00', '19:15', 3);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:00', NULL, 1, 'xia', '14:45', '15:30', 2);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:00', NULL, 1, 'zhone', '11:45', '12:15', 1);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-06 14:18:00', NULL, 1, 'zao', '07:00', '07:45', 0);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-15 15:38:58', NULL, 4, 'xia', '14:00', '16:00', 2);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-15 15:38:58', NULL, 4, 'zhone', '11:45', '12:45', 1);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-15 15:38:58', NULL, 4, 'zao', '06:30', '08:45', 0);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:26:44', NULL, 5, 'zao', '06:30', '08:00', 0);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:26:44', NULL, 5, 'zhone', '11:30', '13:30', 1);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:26:44', NULL, 5, 'xia', '14:45', '15:30', 2);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:26:44', NULL, 5, 'wan', '18:00', '19:30', 3);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:27:59', NULL, 3, 'zao', '06:00', '08:00', 0);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:27:59', NULL, 3, 'zhone', '11:45', '12:30', 1);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:27:59', NULL, 3, 'xia', '15:00', '15:30', 2);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-13 22:27:59', NULL, 3, 'wan', '18:00', '19:00', 3);
INSERT INTO `fb_canteen_time` VALUES ('admin', '2021-01-15 15:38:58', NULL, 4, 'wan', '18:00', '19:30', 3);

-- ----------------------------
-- Table structure for fb_menu
-- ----------------------------
DROP TABLE IF EXISTS `fb_menu`;
CREATE TABLE `fb_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单类型（荤菜、素菜、组合）',
  `menu_img` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图片',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `menu_price` decimal(5, 2) DEFAULT NULL COMMENT '菜单价格',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `canteen_id` bigint(20) DEFAULT NULL COMMENT '食堂编号',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '食堂菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_menu
-- ----------------------------
INSERT INTO `fb_menu` VALUES (1, '红烧肉', 'H', 'http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg', 1, 15.00, '0', '0', '', '2020-12-25 16:02:17', '', NULL, '', NULL);
INSERT INTO `fb_menu` VALUES (2, '红烧排骨', 'H', 'http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg', 0, 20.00, '0', '0', '', '2020-12-25 16:11:26', '', NULL, '', NULL);
INSERT INTO `fb_menu` VALUES (3, '青菜', 'S', 'http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg', 0, 20.00, '0', '0', '', '2020-12-25 16:11:43', '', NULL, '', NULL);
INSERT INTO `fb_menu` VALUES (4, '测试1', 'Z', 'http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg', 0, 20.00, '0', '0', '', '2020-12-25 16:12:00', '', NULL, '', NULL);
INSERT INTO `fb_menu` VALUES (5, '大刀肉', 'H', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif', 0, 20.00, '0', '0', '', '2021-01-23 13:52:25', '', NULL, '', NULL);

-- ----------------------------
-- Table structure for fb_suggest
-- ----------------------------
DROP TABLE IF EXISTS `fb_suggest`;
CREATE TABLE `fb_suggest`  (
  `suggest_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '问题和意见',
  `img` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  `canteen_id` bigint(20) NOT NULL COMMENT '食堂编号',
  `replay` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '回复'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '意见表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_suggest
-- ----------------------------
INSERT INTO `fb_suggest` VALUES ('8730cec0c18c4d2fb02ccbaa4a162a1e', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('1', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('2', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('3', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '');
INSERT INTO `fb_suggest` VALUES ('4', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('5', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('6', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('7', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '');
INSERT INTO `fb_suggest` VALUES ('8', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('9', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('10', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('11', 'wubt', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('12', 'admin', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试', NULL, 3, '');
INSERT INTO `fb_suggest` VALUES ('13', 'admin', '2021-01-08 17:18:03', '', '2021-01-18 10:47:42', '我是测试1', NULL, 3, '没有意见');
INSERT INTO `fb_suggest` VALUES ('b2836280e2c441dca29e22df75f949c3', 'wubt', '2021-01-20 14:01:31', '', NULL, 'cea', 'http://tmp/dQAvcq8D7QWW4fc07bf7480e3b78b355d3d0e54b5dac.jpg,http://tmp/HLP8YBmSc5jOa97af2fb833fe007c69f6ca9f1a5781d.jpg,http://tmp/ZdawlJ3oGxL157a239c1d28ce219aa9c4ea242881a23.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('c754529583a8466e9bb881a6096a0b18', 'wubt', '2021-01-20 14:05:36', '', NULL, 'weewr', 'http://tmp/jUylN0iLsMZQ4fc07bf7480e3b78b355d3d0e54b5dac.jpg,http://tmp/rmmWVgVKu8s8a97af2fb833fe007c69f6ca9f1a5781d.jpg,http://tmp/4txBXrWPQCvV57a239c1d28ce219aa9c4ea242881a23.jpg,http://tmp/wGnkv5Evvc935e558d173e6ee5614ca278ea5c9abf86.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('5e65660054fb4d43ad31508dff3396ec', 'wubt', '2021-01-20 14:10:24', '', NULL, 'wsadf', 'http://tmp/D34np1Ac2Dqda97af2fb833fe007c69f6ca9f1a5781d.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('fb8e9ca5186f44dca50fe183917fb013', 'wubt', '2021-01-20 14:41:38', '', NULL, '太难吃了，怎么做的', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ecaca3b9f9c647d7bd21295bd700e18e.jpg,http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/2e4dacef6f6f41ab9796c56218ba65fa.jpg,http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/5e36d193b2e844b7a17f9c0353ebaffb.jpg,http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ed284fd4325544479faac93a04bcbc26.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('cf3fe1c30e8d4f90a1adb484ecf3cca7', 'jiangl', '2021-01-26 16:53:42', '', NULL, '我是测', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210126/9028378dbc484efbb40dc1fc4ab1ea8d.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('5ee4398189e3443ea99cd1d3b5df6bd0', 'wubt', '2021-01-26 18:17:46', '', NULL, '啊啊啊', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210126/a88d441c7f784e52a8c5cca57c61aeba.jpg', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('c1910c627aa14a0a94fab974913c1dbd', 'wenfl', '2021-01-28 21:56:13', '', NULL, '消费输不了小数点', '', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('e237a7079e8a4b8f8b9373a0fe65c032', 'wenfl', '2021-01-28 21:57:34', '', NULL, '订不了餐', '', 3, NULL);
INSERT INTO `fb_suggest` VALUES ('74247d08e53c412f8c8252cd9899548d', 'dingw', '2021-01-28 22:26:55', '', NULL, '啊啊啊', '', 3, NULL);

-- ----------------------------
-- Table structure for fb_trade_info
-- ----------------------------
DROP TABLE IF EXISTS `fb_trade_info`;
CREATE TABLE `fb_trade_info`  (
  `trade_Time` datetime(0) DEFAULT NULL COMMENT '交易时间',
  `trade_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '交易IP',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `trade_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交易ID',
  `trade_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交易类型',
  `payment` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易金额',
  `canteen_id` bigint(20) DEFAULT NULL COMMENT '食堂编号',
  PRIMARY KEY (`trade_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '交易记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_trade_info
-- ----------------------------
INSERT INTO `fb_trade_info` VALUES ('2021-01-09 22:54:05', '192.168.31.236', 'admin', '2021-01-09 22:54:05', '', NULL, NULL, 1, 'abf8d34425944b48be7efe96b68aa649', 'xiao', '-15', 1);
INSERT INTO `fb_trade_info` VALUES ('2021-01-09 22:54:38', '192.168.31.236', 'admin', '2021-01-09 22:54:38', '', NULL, NULL, 1, '9b44c6ff46824bf9866f1de47b3f8ed6', 'xiao', '-15', 1);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 16:49:28', '192.168.4.11', 'admin', '2021-01-12 16:49:28', '', NULL, NULL, 3, '8b01fcaad71d41c7a0cb25123c59511d', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 16:49:40', '192.168.4.11', 'admin', '2021-01-12 16:49:40', '', NULL, NULL, 3, '433922d0a7db4a8cbf08cee9630b8e10', 'chong', '+20', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:18:49', '192.168.4.11', 'admin', '2021-01-12 17:18:49', '', NULL, NULL, 1, 'beca4f82d39041319a606ca299f94f4b', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:07', '192.168.4.11', 'admin', '2021-01-12 17:19:07', '', NULL, NULL, 3, 'c7328343bde8467e84e0c0bae5c5d76f', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 4, 'd7ab4e8042ec44a385b01d63bbc69a0e', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, 'c3f5b504f148431b91e5ff8c03b09a36', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 6, '257d915e5b0844e8b27ec1a09e50d6ab', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 7, '8f6bcf3e24984ed6b8044b66108e8a1c', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:15', '192.168.4.11', 'admin', '2021-01-12 17:35:15', '', NULL, NULL, 1, 'a0b86c4dffd64ecdbe8b6b1eee4ff6dd', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:15', '192.168.4.11', 'admin', '2021-01-12 17:35:15', '', NULL, NULL, 3, '6fc185f58ae640f5b75336698fda7594', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:15', '192.168.4.11', 'admin', '2021-01-12 17:35:15', '', NULL, NULL, 4, '70eab65a03e64515a21ddc5a5102ed42', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:15', '192.168.4.11', 'admin', '2021-01-12 17:35:15', '', NULL, NULL, 5, '1cb4cad3148f4b6ea1af1b81422540fa', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:16', '192.168.4.11', 'admin', '2021-01-12 17:35:16', '', NULL, NULL, 6, 'e9c366bfaf574f48b3cb9cdf3e76f990', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:35:16', '192.168.4.11', 'admin', '2021-01-12 17:35:16', '', NULL, NULL, 7, '5f9caad4ef044a3f8c4353aa16269b2e', 'chong', '+10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:36:12', '192.168.4.11', 'admin', '2021-01-12 17:36:12', '', NULL, NULL, 6, 'd26685b8a2564e41a52bf179bf01f0bc', 'chong', '+20', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, 'c3f5b504f148431b91e5ff8c03b09a361', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '12', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '122', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '32e', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '121212', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '12213', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, 'ewsq1', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '334', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, '54543', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, 'weee', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-12 17:19:10', '192.168.4.11', 'admin', '2021-01-12 17:19:10', '', NULL, NULL, 5, 'ererer', 'zao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-25 18:21:04', '192.168.4.11', 'wubt', '2021-01-25 18:21:04', '', NULL, '', 5, 'dce3afe8755a4842a42c467e5b4f6b9e', 'xiao', '-1', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-26 18:16:23', '192.168.4.11', 'wubt', '2021-01-26 18:16:23', '', NULL, '', 5, 'cb3e7090395e423b81cc38b55cd6774c', 'xiao', '-0', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-26 19:09:37', '192.168.4.11', 'wubt', '2021-01-26 19:09:37', '', NULL, '', 5, 'da67f941f5f442d1b959bb19e0a1c328', 'xiao', '-0', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-26 19:09:38', '192.168.4.11', 'wubt', '2021-01-26 19:09:38', '', NULL, '', 5, '794275b7b928435c965bcc8e8ea9442f', 'xiao', '-0', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-28 21:55:51', '127.0.0.1', 'wenfl', '2021-01-28 21:55:51', '', NULL, '', 3, 'faf7d57c8c70476e8ee460890d5f049b', 'xiao', '-05', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-01-28 23:08:45', '127.0.0.1', 'jiangl', '2021-01-28 23:08:45', '', NULL, '', 4, '6fb8a84d3a874fd5b44567476b5e0b49', 'xiao', '-10', NULL);
INSERT INTO `fb_trade_info` VALUES ('2021-02-01 19:01:55', '127.0.0.1', 'admin', '2021-02-01 19:01:55', '', NULL, NULL, 1, '1988651bc78643a8a129ac337bc330eb', 'chong', '+10', NULL);

-- ----------------------------
-- Table structure for fb_wallet
-- ----------------------------
DROP TABLE IF EXISTS `fb_wallet`;
CREATE TABLE `fb_wallet`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `balance` decimal(10, 2) DEFAULT NULL COMMENT '余额'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '余额表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fb_wallet
-- ----------------------------
INSERT INTO `fb_wallet` VALUES (1, '', NULL, 'admin', '2021-02-01 19:01:54', NULL, 40.00);
INSERT INTO `fb_wallet` VALUES (3, 'admin', '2021-01-12 17:35:15', 'wenfl', '2021-01-28 21:55:51', NULL, 5.00);
INSERT INTO `fb_wallet` VALUES (4, 'admin', '2021-01-12 17:35:15', 'jiangl', '2021-01-28 23:08:45', NULL, 0.00);
INSERT INTO `fb_wallet` VALUES (5, 'admin', '2021-01-12 17:35:15', 'wubt', '2021-01-26 19:09:38', NULL, 9.00);
INSERT INTO `fb_wallet` VALUES (6, 'admin', '2021-01-12 17:35:15', 'admin', '2021-01-12 17:36:12', NULL, 30.00);
INSERT INTO `fb_wallet` VALUES (7, 'admin', '2021-01-12 17:35:16', '', NULL, NULL, 10.00);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'fb_canteen', '食堂表', 'Canteen', 'crud', 'com.wuhuacloud.fb', 'fb', 'canteen', '食堂', 'wenfl', '0', '/', '{\"parentMenuId\":\"1061\"}', 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30', NULL);
INSERT INTO `gen_table` VALUES (3, 'fb_menu', '食堂菜单表', 'Menu', 'crud', 'com.wuhuacloud.fb', 'fb', 'menu', '食堂菜单', 'wenfl', '0', '/', '{\"parentMenuId\":\"1061\"}', 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56', NULL);
INSERT INTO `gen_table` VALUES (4, 'fb_suggest', '意见表', 'Suggest', 'crud', 'com.wuhuacloud.fb', 'fb', 'suggest', '意见', 'wenfl', '1', 'E:\\wfl\\IdeaProjects\\fb\\fb-business\\src\\main\\java\\', '{\"parentMenuId\":1061}', 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13', NULL);
INSERT INTO `gen_table` VALUES (5, 'sys_file_info', '文件信息表', 'SysFileInfo', 'crud', 'com.wuhuacloud.system', 'system', 'fileinfo', '文件信息', 'wenfl', '0', '/', '{}', 'admin', '2020-12-25 16:58:38', '', '2020-12-25 17:00:59', NULL);
INSERT INTO `gen_table` VALUES (7, 'fb_canteen_menu', '食堂与菜单关联表', 'CanteenMenu', 'crud', 'com.wuhuacloud.fb', 'fb', 'menu', '食堂与菜单关联', 'wenfl', '0', '/', NULL, 'admin', '2020-12-29 00:54:01', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (8, 'fb_trade_info', '交易记录表', 'TradeInfo', 'crud', 'com.wuhuacloud.fb', 'fb', 'info', '交易记录', 'wenfl', '0', '/', '{\"parentMenuId\":1061}', 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:26', NULL);
INSERT INTO `gen_table` VALUES (9, 'fb_wallet', '余额表', 'Wallet', 'crud', 'com.wuhuacloud.fb', 'fb', 'wallet', '余额', 'wenfl', '0', '/', '{\"parentMenuId\":1061}', 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06', NULL);
INSERT INTO `gen_table` VALUES (12, 'fb_canteen_time', '食堂营业时间', 'CanteenTime', 'crud', 'com.wuhuacloud.fb', 'fb', 'time', '食堂营业时间', 'wenfl', '0', '/', NULL, 'admin', '2021-01-06 13:47:32', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '食堂编号', 'bigint(20)', 'Long', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (2, '1', 'canteen_name', '食堂名称', 'varchar(30)', 'String', 'canteenName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (3, '1', 'canteen_tel', '食堂电话', 'varchar(30)', 'String', 'canteenTel', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (4, '1', 'canteen_addr', '食堂地址', 'varchar(255)', 'String', 'canteenAddr', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (5, '1', 'canteen_principal', '负责人', 'varchar(10)', 'String', 'canteenPrincipal', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (6, '1', 'canteen_longitude', '食堂经度', 'decimal(10,7)', 'BigDecimal', 'canteenLongitude', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (7, '1', 'canteen_latitude', '食堂纬度', 'decimal(10,7)', 'BigDecimal', 'canteenLatitude', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (8, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (9, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (10, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (11, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (12, '1', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 12, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (13, '1', 'canteen_sort', '显示顺序', 'int(4)', 'Integer', 'canteenSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (14, '1', 'data_scope', '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）', 'char(1)', 'String', 'dataScope', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (15, '1', 'canteen_status', '状态（0正常 1停用）', 'char(1)', 'String', 'canteenStatus', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', 'sys_common_status', 15, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (16, '1', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 16, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 22:22:30');
INSERT INTO `gen_table_column` VALUES (21, '3', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '1', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (22, '3', 'menu_name', '菜单名称', 'varchar(30)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (23, '3', 'menu_type', '菜单类型（荤菜、素菜、组合）', 'char(1)', 'String', 'menuType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 'fb_menu_type', 3, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (24, '3', 'menu_img', '菜单图片', 'varchar(100)', 'String', 'menuImg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'uploadImage', '', 4, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (25, '3', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (26, '3', 'menu_price', '菜单价格', 'decimal(5,2)', 'BigDecimal', 'menuPrice', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (27, '3', 'visible', '显示状态（0显示 1隐藏）', 'char(1)', 'String', 'visible', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'sys_show_hide', 7, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (28, '3', 'status', '菜单状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', 'sys_common_status', 8, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (29, '3', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (30, '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (31, '3', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (32, '3', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (33, '3', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 13, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (34, '3', 'canteen_id', '食堂编号', 'bigint(20)', 'Long', 'canteenId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2020-12-23 15:19:01', '', '2020-12-25 15:05:56');
INSERT INTO `gen_table_column` VALUES (35, '4', 'suggest_id', 'ID', 'bigint(20)', 'Long', 'suggestId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (36, '4', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (37, '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 3, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (38, '4', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (39, '4', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (40, '4', 'remark', '问题和意见', 'varchar(500)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'textarea', '', 6, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (41, '4', 'img', '图片', 'varchar(255)', 'String', 'img', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (42, '4', 'canteen_id', '食堂编号', 'bigint(20)', 'Long', 'canteenId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2020-12-23 15:19:01', '', '2020-12-23 15:59:13');
INSERT INTO `gen_table_column` VALUES (43, '5', 'file_id', '文件id', 'int(11)', 'Long', 'fileId', '1', '1', NULL, NULL, NULL, '1', NULL, 'EQ', 'input', '', 1, 'admin', '2020-12-25 16:58:38', '', '2020-12-25 17:00:59');
INSERT INTO `gen_table_column` VALUES (44, '5', 'file_name', '文件名称', 'varchar(50)', 'String', 'fileName', '0', '0', NULL, NULL, NULL, '1', '1', 'LIKE', 'input', '', 2, 'admin', '2020-12-25 16:58:38', '', '2020-12-25 17:00:59');
INSERT INTO `gen_table_column` VALUES (45, '5', 'file_path', '文件路径', 'varchar(255)', 'String', 'filePath', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'input', '', 3, 'admin', '2020-12-25 16:58:38', '', '2020-12-25 17:00:59');
INSERT INTO `gen_table_column` VALUES (46, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'datetime', '', 4, 'admin', '2020-12-25 16:58:38', '', '2020-12-25 17:00:59');
INSERT INTO `gen_table_column` VALUES (56, '7', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 1, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (57, '7', 'canteen_id', '食堂编号', 'bigint(20)', 'Long', 'canteenId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (58, '7', 'meal_type', '餐类型（早中晚下午茶）', 'varchar(10)', 'String', 'mealType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 3, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '7', 'meal_date', '日期', 'varchar(10)', 'String', 'mealDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (60, '7', 'menu_name', '菜单名称', 'varchar(30)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (61, '7', 'menu_type', '菜单类型（荤菜、素菜、组合）', 'char(1)', 'String', 'menuType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 6, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (62, '7', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (63, '7', 'menu_price', '菜单价格', 'decimal(5,2)', 'BigDecimal', 'menuPrice', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (64, '7', 'canteen_price', '食堂价格', 'decimal(5,2)', 'BigDecimal', 'canteenPrice', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (65, '7', 'menu_img', '菜单图片', 'varchar(100)', 'String', 'menuImg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2020-12-29 00:54:01', '', NULL);
INSERT INTO `gen_table_column` VALUES (66, '8', 'trade_dime', '交易时间', 'datetime', 'Date', 'tradeDime', '0', '0', NULL, NULL, NULL, '1', '1', 'EQ', 'datetime', '', 1, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:26');
INSERT INTO `gen_table_column` VALUES (67, '8', 'trade_ip', '交易IP', 'varchar(50)', 'String', 'tradeIp', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'input', '', 2, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (68, '8', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 3, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (69, '8', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (70, '8', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (71, '8', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (72, '8', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'textarea', '', 7, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (73, '8', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', '1', NULL, NULL, '1', '1', 'EQ', 'input', '', 8, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (74, '8', 'trade_id', '交易ID', 'bigint(20)', 'Long', 'tradeId', '1', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'input', '', 9, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (75, '8', 'trade_type', '交易类型', 'varchar(2)', 'String', 'tradeType', '0', '0', NULL, NULL, NULL, '1', '1', 'EQ', 'select', 'trade_type', 10, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (76, '8', 'payment', '交易金额', 'varchar(10)', 'String', 'payment', '0', '0', NULL, NULL, NULL, '1', '1', 'EQ', 'input', '', 11, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:27');
INSERT INTO `gen_table_column` VALUES (77, '9', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '0', '0', '1', NULL, NULL, '1', '1', 'EQ', 'input', '', 1, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (78, '9', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (79, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 3, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (80, '9', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (81, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (82, '9', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, NULL, NULL, '1', NULL, 'EQ', 'textarea', '', 6, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (83, '9', 'balance', '余额', 'decimal(10,2)', 'BigDecimal', 'balance', '0', '0', NULL, NULL, NULL, '1', '1', 'EQ', 'input', '', 7, 'admin', '2021-01-04 13:42:32', '', '2021-01-04 13:46:06');
INSERT INTO `gen_table_column` VALUES (98, '12', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (99, '12', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 2, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (100, '12', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 3, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (101, '12', 'canteen_id', '食堂编号', 'bigint(20)', 'Long', 'canteenId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (102, '12', 'meal_type', '餐类型（早中晚下午茶）', 'varchar(10)', 'String', 'mealType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (103, '12', 'start_time', '开始时间', 'varchar(10)', 'String', 'startTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (104, '12', 'stop_time', '结束时间', 'varchar(10)', 'String', 'stopTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (105, '12', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2021-01-06 13:47:32', '', NULL);
INSERT INTO `gen_table_column` VALUES (106, '4', 'replay', '回复', 'varchar(500)', 'String', 'replay', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 9, '', '2021-01-08 16:43:09', '', NULL);
INSERT INTO `gen_table_column` VALUES (107, '8', 'canteen_id', '食堂编号', 'bigint(20)', 'Long', 'canteenId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 12, '', '2021-01-09 21:59:21', '', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.wuhuacloud.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720023636F6D2E7775687561636C6F75642E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002C636F6D2E7775687561636C6F75642E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017684B5FD0078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.wuhuacloud.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720023636F6D2E7775687561636C6F75642E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002C636F6D2E7775687561636C6F75642E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017684B5FD0078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.wuhuacloud.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720023636F6D2E7775687561636C6F75642E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002C636F6D2E7775687561636C6F75642E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017684B5FD0078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'VM-16-10-centos1621839283108', 1621841781002, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1621839290000, -1, 5, 'PAUSED', 'CRON', 1621839283000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1621839285000, -1, 5, 'PAUSED', 'CRON', 1621839285000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1621839300000, -1, 5, 'PAUSED', 'CRON', 1621839286000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2020-12-21 17:51:28', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2020-12-21 17:51:28', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2020-12-21 17:51:28', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '云存储配置', 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"http://qn6her7ha.hn-bkt.clouddn.com\",\"qiniuPrefix\":\"upload1\",\"qiniuAccessKey\":\"oIE7MMVKeYtKBJcr7g7HvwT36iu3FWlmbtcpOxJj\",\"qiniuSecretKey\":\"uOQiBfJugkoGgBSlOrI2pEy9dL83y2ul8gijC4Fg\",\"qiniuBucketName\":\"wflfb\",\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', 'N', 'admin', NULL, '', '2021-01-19 19:08:28', NULL);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '广五集团', 0, '温生', '13800138000', '', '0', '0', 'admin', '2020-12-21 17:51:27', 'admin', '2021-01-12 10:09:10');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '广东五华云计算有限公司', 1, '', '', '', '0', '0', 'admin', '2020-12-21 17:51:27', 'admin', '2021-01-12 10:09:10');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2020-12-21 17:51:27', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 0, '荤菜', 'H', 'fb_menu_type', NULL, NULL, 'N', '0', 'admin', '2020-12-25 11:44:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (30, 1, '素菜', 'S', 'fb_menu_type', NULL, NULL, 'N', '0', 'admin', '2020-12-25 11:45:00', 'admin', '2020-12-25 11:45:07', NULL);
INSERT INTO `sys_dict_data` VALUES (31, 2, '组合', 'Z', 'fb_menu_type', NULL, NULL, 'N', '0', 'admin', '2020-12-25 11:45:20', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (32, 0, '早餐', 'zao', 'fb_meal_type', NULL, NULL, 'N', '0', 'admin', '2020-12-26 14:27:46', 'admin', '2020-12-26 14:57:58', NULL);
INSERT INTO `sys_dict_data` VALUES (33, 1, '中餐', 'zhone', 'fb_meal_type', NULL, NULL, 'N', '0', 'admin', '2020-12-26 14:28:03', 'admin', '2020-12-26 14:58:04', NULL);
INSERT INTO `sys_dict_data` VALUES (34, 2, '下午茶', 'xia', 'fb_meal_type', NULL, NULL, 'N', '0', 'admin', '2020-12-26 14:28:19', 'admin', '2020-12-26 14:58:13', NULL);
INSERT INTO `sys_dict_data` VALUES (35, 3, '晚餐', 'wan', 'fb_meal_type', NULL, NULL, 'N', '0', 'admin', '2020-12-26 14:28:40', 'admin', '2020-12-26 14:58:20', NULL);
INSERT INTO `sys_dict_data` VALUES (36, 0, '早餐', 'zao', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:33:49', 'admin', '2021-01-04 11:35:33', NULL);
INSERT INTO `sys_dict_data` VALUES (37, 1, '午餐', 'wu', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:34:00', 'admin', '2021-01-04 11:35:38', NULL);
INSERT INTO `sys_dict_data` VALUES (38, 2, '晚餐', 'wan', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:34:15', 'admin', '2021-01-04 11:35:48', NULL);
INSERT INTO `sys_dict_data` VALUES (39, 4, '小卖部', 'xiao', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:34:29', 'admin', '2021-01-04 11:35:58', NULL);
INSERT INTO `sys_dict_data` VALUES (40, 4, '充值', 'chong', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:37:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (41, 5, '津贴', 'jing', 'trade_type', NULL, NULL, 'N', '0', 'admin', '2021-01-04 11:38:16', 'admin', '2021-01-04 11:38:21', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2020-12-21 17:51:28', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '食堂菜单类型', 'fb_menu_type', '0', 'admin', '2020-12-25 11:43:55', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (12, '餐类别', 'fb_meal_type', '0', 'admin', '2020-12-26 14:27:24', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (13, '交易类型', 'trade_type', '0', 'admin', '2021-01-04 11:32:34', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_file_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_file_info`;
CREATE TABLE `sys_file_info`  (
  `file_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `file_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文件名称',
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '文件路径',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file_info
-- ----------------------------
INSERT INTO `sys_file_info` VALUES (1, 'file', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210119/2cfb6ca5def5419cbaf4dc5af62d18be.jpg', '2021-01-19 19:14:35', 'admin');
INSERT INTO `sys_file_info` VALUES (2, 'file', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210119/e08e97775da740f9b867f0188a500256.jpg', '2021-01-19 19:16:18', 'admin');
INSERT INTO `sys_file_info` VALUES (3, '青菜.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210119/da8cf26a96114f028e011df9156c30eb.jpg', '2021-01-19 19:18:10', 'admin');
INSERT INTO `sys_file_info` VALUES (4, 'cLnGG7I6lR1o4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/1c2b055304854cb49a0970882658c448.jpg', '2021-01-20 10:06:52', 'wubt');
INSERT INTO `sys_file_info` VALUES (5, 'Tu82nulwDxQT57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/65d8684d3d10487aa0537621518c84cf.jpg', '2021-01-20 10:09:47', 'wubt');
INSERT INTO `sys_file_info` VALUES (6, 'XBwWfyeeFIV45e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/fcb9582f161c47259075141a8eed2739.jpg', '2021-01-20 10:09:48', 'wubt');
INSERT INTO `sys_file_info` VALUES (7, 'dGJ5qdRjJima4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/c3cf3bcaee5a462b8ff50706a844e392.jpg', '2021-01-20 10:11:24', 'wubt');
INSERT INTO `sys_file_info` VALUES (8, 'C5lfYzcH0mQza97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/52a13be2fa2a485ca64bc7341b75286f.jpg', '2021-01-20 10:11:25', 'wubt');
INSERT INTO `sys_file_info` VALUES (9, 'gB92yOrRgWnP57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/800e4ddf0e0649f7b61f58ee71695a1f.jpg', '2021-01-20 10:11:25', 'wubt');
INSERT INTO `sys_file_info` VALUES (10, 'bu9hLcMfF9STa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/0ab4f89749a44c17af2d14be56f6de12.jpg', '2021-01-20 10:19:04', 'wubt');
INSERT INTO `sys_file_info` VALUES (11, 'vwN1YQhDvQX257a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/e94e67ec30e14c168ebaebad7eaa6900.jpg', '2021-01-20 10:19:05', 'wubt');
INSERT INTO `sys_file_info` VALUES (12, 'ssxS45v9HBQS4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/1df1b398f3d54b1993975facc2bc5dad.jpg', '2021-01-20 10:21:59', 'wubt');
INSERT INTO `sys_file_info` VALUES (13, 'zrgjaogCwLIZa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/864a3cd1faa747ca9c0424b4eea380ca.jpg', '2021-01-20 10:21:59', 'wubt');
INSERT INTO `sys_file_info` VALUES (14, 'NZ8ewcuFwaPn57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/fea9d36df492454aa0e55ee49dae60f0.jpg', '2021-01-20 10:21:59', 'wubt');
INSERT INTO `sys_file_info` VALUES (15, 'Jto9CNwte5BE5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/c0f488ce306e47eda1e245bf8643d391.jpg', '2021-01-20 10:22:00', 'wubt');
INSERT INTO `sys_file_info` VALUES (16, 'ohjMHkZ9MRDma97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/f6807746a6af4eaaabca9aef1abbd621.jpg', '2021-01-20 10:23:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (17, 'f7U4NTlai3Zg57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ad67cf98f6d84cd0ae203321a98bd858.jpg', '2021-01-20 10:23:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (18, 'QhdF78QMv6uF5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/793c33a37c644cfb9ca413045e77a0ea.jpg', '2021-01-20 10:23:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (19, 'Sbkj3mtNt41ga97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/7a2a89448ba54103a7efbded65875f78.jpg', '2021-01-20 10:24:40', 'wubt');
INSERT INTO `sys_file_info` VALUES (20, 'sEjtrkuAFH9z57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/11fa2da99d7442e5bf5e34c05ec7b32a.jpg', '2021-01-20 10:24:40', 'wubt');
INSERT INTO `sys_file_info` VALUES (21, 'yjpAPNqAZ4R55e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/541dde45aa4a4c12a7cbb0be099359aa.jpg', '2021-01-20 10:24:41', 'wubt');
INSERT INTO `sys_file_info` VALUES (22, 'AGt9ieW60Y9F4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/6e2960729e6c45748aa6a262848ea7ec.jpg', '2021-01-20 12:47:11', 'wubt');
INSERT INTO `sys_file_info` VALUES (23, 'KeTft7FUDBq1a97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/f84d298e51964aef97eb61dbd02ff83d.jpg', '2021-01-20 12:50:54', 'wubt');
INSERT INTO `sys_file_info` VALUES (24, 'AwyXV78DoOpz57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/2b8e95f14f674e26bf5ac61e58b8f601.jpg', '2021-01-20 12:50:55', 'wubt');
INSERT INTO `sys_file_info` VALUES (25, 'GNFVxCfPbVsM5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/d62bc63745344361806f909fa506e838.jpg', '2021-01-20 12:50:55', 'wubt');
INSERT INTO `sys_file_info` VALUES (26, 'kPlMLuRyxikS4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/725b08641ef1421ca8132cbc5e47f970.jpg', '2021-01-20 12:52:07', 'wubt');
INSERT INTO `sys_file_info` VALUES (27, 'fqnPciGjBW2ga97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/94e69257b8054850912eef26c3ea90cc.jpg', '2021-01-20 12:52:07', 'wubt');
INSERT INTO `sys_file_info` VALUES (28, 'eoa3QInc0OI157a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/9ecb5f5e52df4620bfd3e96086940836.jpg', '2021-01-20 12:52:08', 'wubt');
INSERT INTO `sys_file_info` VALUES (29, 'aylzAhG0HK025e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/1331d45640a74fdc951c48055e9596cf.jpg', '2021-01-20 12:52:08', 'wubt');
INSERT INTO `sys_file_info` VALUES (30, 'ZTLNvX8NuPmV4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/b6ec3d8d10c64cbe85f7c22938b6a960.jpg', '2021-01-20 13:00:52', 'wubt');
INSERT INTO `sys_file_info` VALUES (31, 'SJbqrMnTsfDLa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/8450623287fe49efbf64e461f51ce188.jpg', '2021-01-20 13:00:52', 'wubt');
INSERT INTO `sys_file_info` VALUES (32, 'zh0igZMVPqnl57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/944e8fecf6884dbcb61b826a4da0690d.jpg', '2021-01-20 13:00:52', 'wubt');
INSERT INTO `sys_file_info` VALUES (33, '4uIiNXMaRsq95e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/bdd5ffb55ba8474683ee1f317bb1f19c.jpg', '2021-01-20 13:00:53', 'wubt');
INSERT INTO `sys_file_info` VALUES (34, 'ZUjxKmBbdXBa4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/10f9abcce00e477da76e833b1ccea50e.jpg', '2021-01-20 13:03:02', 'wubt');
INSERT INTO `sys_file_info` VALUES (35, 'dOlou80djUt8a97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/715f586fadfb416bb51a5dee62828b75.jpg', '2021-01-20 13:03:02', 'wubt');
INSERT INTO `sys_file_info` VALUES (36, 'AhPi8fLp1g0D57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ef32da49414c46b89e8e31903ccad811.jpg', '2021-01-20 13:03:02', 'wubt');
INSERT INTO `sys_file_info` VALUES (37, '1SrMvjclHXRs5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/8369c1df1f34413aa770e6c0fae1c2cf.jpg', '2021-01-20 13:03:03', 'wubt');
INSERT INTO `sys_file_info` VALUES (38, 'PaZQVAZSE6HN4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/92def942196c4f088be4fcd6be7da2b1.jpg', '2021-01-20 13:09:37', 'wubt');
INSERT INTO `sys_file_info` VALUES (39, 'QFtJLEfLOfuDa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/aa1e2892a3714d4fbceae9018ba1bd87.jpg', '2021-01-20 13:09:37', 'wubt');
INSERT INTO `sys_file_info` VALUES (40, 'brunAPtoth3C57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/facbde1a51a64aa7b30974358a2ddbd3.jpg', '2021-01-20 13:09:37', 'wubt');
INSERT INTO `sys_file_info` VALUES (41, 'aIFqXqeS1OVZ5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/070f698523e94b4da621dafb1970c664.jpg', '2021-01-20 13:09:38', 'wubt');
INSERT INTO `sys_file_info` VALUES (42, 'JziqpaEPFqw557a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/87f2a281379d44ae8141100f12557086.jpg', '2021-01-20 13:34:32', 'wubt');
INSERT INTO `sys_file_info` VALUES (43, '4GUEvhKovtng5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/698168b458f6480cb44cc2831d30062a.jpg', '2021-01-20 13:34:33', 'wubt');
INSERT INTO `sys_file_info` VALUES (44, 'Q2eh28jkPyHN4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/95231c6345c646fbaea0155d6d3ede4d.jpg', '2021-01-20 13:37:44', 'wubt');
INSERT INTO `sys_file_info` VALUES (45, 'TO96TCWdIGzOa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/073937d41f1542e9ab28adbc7443cd63.jpg', '2021-01-20 13:37:45', 'wubt');
INSERT INTO `sys_file_info` VALUES (46, '0XpF4UtJYEiG57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/3b0cc159db624ff48754842e7587effd.jpg', '2021-01-20 13:37:45', 'wubt');
INSERT INTO `sys_file_info` VALUES (47, 'fWAifVca3pwU5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/0218eef2cd204bc28e127c2162ec1dd5.jpg', '2021-01-20 13:37:45', 'wubt');
INSERT INTO `sys_file_info` VALUES (48, 'hq7w7fM9CKuAa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/eb6f8d2365754429b78c62c30e064f27.jpg', '2021-01-20 13:50:12', 'wubt');
INSERT INTO `sys_file_info` VALUES (49, 'wkSfCDoxCWBN57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/b750d1c9817b44a7a1bcb7b61052e6be.jpg', '2021-01-20 13:50:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (50, 'UrREP63KeCLI5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/d2277304a0e24e66bb973a7b29bd737f.jpg', '2021-01-20 13:50:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (51, 'c7lQqB0PTwNh4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/f05c202aae204e45a1cc1ae8f038b089.jpg', '2021-01-20 13:53:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (52, '6iQhbarXtafDa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/baffa99f8f584ba0a296a4794dc3f96d.jpg', '2021-01-20 13:53:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (53, 'g8qNQRoOzoFr57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/0906375b2d8e43e4af5583eaf45dd987.jpg', '2021-01-20 13:53:13', 'wubt');
INSERT INTO `sys_file_info` VALUES (54, 'tFmXM8uWwwww5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/7817dd9ed39646a6a3bdee13025d1b0a.jpg', '2021-01-20 13:53:14', 'wubt');
INSERT INTO `sys_file_info` VALUES (55, 'E2MCtQ96BPyS4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/94c5856017ee4730a2cc9eac9afb502e.jpg', '2021-01-20 13:58:33', 'wubt');
INSERT INTO `sys_file_info` VALUES (56, 'pVrodqfq6tD7a97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/9abc16e7a9b14ba68b7878d34914ea9a.jpg', '2021-01-20 13:58:33', 'wubt');
INSERT INTO `sys_file_info` VALUES (57, 'D6l2OBd8D0B357a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/0935dd5bb9e144c38421fd2c3dda2d57.jpg', '2021-01-20 13:58:34', 'wubt');
INSERT INTO `sys_file_info` VALUES (58, 'V96FKDufJdiH5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/829b990e5d4540428739a24a099847ac.jpg', '2021-01-20 13:58:34', 'wubt');
INSERT INTO `sys_file_info` VALUES (59, 'dQAvcq8D7QWW4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/f1ab3e268d704913ad39127e257787ce.jpg', '2021-01-20 14:01:22', 'wubt');
INSERT INTO `sys_file_info` VALUES (60, 'HLP8YBmSc5jOa97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/7759b5b9d1e04b6cb86db8faa3fc34d2.jpg', '2021-01-20 14:01:23', 'wubt');
INSERT INTO `sys_file_info` VALUES (61, 'ZdawlJ3oGxL157a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/1679e0c4a41448a5ac7c07c79af9b936.jpg', '2021-01-20 14:01:23', 'wubt');
INSERT INTO `sys_file_info` VALUES (62, 'jUylN0iLsMZQ4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ccffb6b81492422bada29bbc03cb6aed.jpg', '2021-01-20 14:05:33', 'wubt');
INSERT INTO `sys_file_info` VALUES (63, 'rmmWVgVKu8s8a97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/b92d8aa1d2f7443eb9f1cc909a7615d6.jpg', '2021-01-20 14:05:34', 'wubt');
INSERT INTO `sys_file_info` VALUES (64, '4txBXrWPQCvV57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/e614f478d2ee449baef653a941f0cef6.jpg', '2021-01-20 14:05:34', 'wubt');
INSERT INTO `sys_file_info` VALUES (65, 'wGnkv5Evvc935e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/5687bdfae7d94d9ba1e8e5aa5c76c1e7.jpg', '2021-01-20 14:05:35', 'wubt');
INSERT INTO `sys_file_info` VALUES (66, 'D34np1Ac2Dqda97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/897ca8f2464b4967b5ef7b1325e81960.jpg', '2021-01-20 14:10:24', 'wubt');
INSERT INTO `sys_file_info` VALUES (67, '2NoThqnjetUL57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/682b40a19ae54785a8a715631dfe5f18.jpg', '2021-01-20 14:10:25', 'wubt');
INSERT INTO `sys_file_info` VALUES (68, 'STNiXzjcgnUx5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/c9b53338ad2d40a38f5b539ff67f0660.jpg', '2021-01-20 14:10:25', 'wubt');
INSERT INTO `sys_file_info` VALUES (69, 'monGTUvDcg7n4fc07bf7480e3b78b355d3d0e54b5dac.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ecaca3b9f9c647d7bd21295bd700e18e.jpg', '2021-01-20 14:41:03', 'wubt');
INSERT INTO `sys_file_info` VALUES (70, 'x4e2sHWfEqvna97af2fb833fe007c69f6ca9f1a5781d.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/2e4dacef6f6f41ab9796c56218ba65fa.jpg', '2021-01-20 14:41:03', 'wubt');
INSERT INTO `sys_file_info` VALUES (71, 'nmBcceUjJ35K57a239c1d28ce219aa9c4ea242881a23.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/5e36d193b2e844b7a17f9c0353ebaffb.jpg', '2021-01-20 14:41:04', 'wubt');
INSERT INTO `sys_file_info` VALUES (72, '9v9b46YAtysO5e558d173e6ee5614ca278ea5c9abf86.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210120/ed284fd4325544479faac93a04bcbc26.jpg', '2021-01-20 14:41:04', 'wubt');
INSERT INTO `sys_file_info` VALUES (73, '下载.jfif', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif', '2021-01-23 13:52:15', 'admin');
INSERT INTO `sys_file_info` VALUES (74, 'tmp_c2825943802fdfc8b8cf8065710bd921.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210126/9028378dbc484efbb40dc1fc4ab1ea8d.jpg', '2021-01-26 16:53:37', 'jiangl');
INSERT INTO `sys_file_info` VALUES (75, 'tmp_42f7d61f5f7b511ae458b3b1d65ebbd5.jpg', 'http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210126/a88d441c7f784e52a8c5cca57c61aeba.jpg', '2021-01-26 18:17:45', 'wubt');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2020-12-21 17:51:28', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：2毫秒', '0', '', '2021-01-09 22:44:40');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 538 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-12-22 23:16:59');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-23 15:12:02');
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-23 15:53:23');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-23 18:04:57');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2020-12-23 18:05:42');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-23 18:05:46');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-23 21:57:28');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-12-24 15:54:09');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-25 10:18:42');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-25 10:18:46');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-25 11:37:33');
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-25 12:30:18');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-25 13:42:00');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-25 13:42:02');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-25 14:13:20');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-26 12:24:17');
INSERT INTO `sys_logininfor` VALUES (17, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-26 17:29:50');
INSERT INTO `sys_logininfor` VALUES (18, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-26 17:29:57');
INSERT INTO `sys_logininfor` VALUES (19, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-26 22:05:04');
INSERT INTO `sys_logininfor` VALUES (20, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-26 22:55:40');
INSERT INTO `sys_logininfor` VALUES (21, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 00:21:31');
INSERT INTO `sys_logininfor` VALUES (22, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-27 14:19:59');
INSERT INTO `sys_logininfor` VALUES (23, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-27 14:20:06');
INSERT INTO `sys_logininfor` VALUES (24, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 14:20:10');
INSERT INTO `sys_logininfor` VALUES (25, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 17:41:56');
INSERT INTO `sys_logininfor` VALUES (26, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-27 18:29:19');
INSERT INTO `sys_logininfor` VALUES (27, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 18:29:23');
INSERT INTO `sys_logininfor` VALUES (28, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 21:01:36');
INSERT INTO `sys_logininfor` VALUES (29, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-27 22:49:31');
INSERT INTO `sys_logininfor` VALUES (30, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 14:11:14');
INSERT INTO `sys_logininfor` VALUES (31, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 15:15:37');
INSERT INTO `sys_logininfor` VALUES (32, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 15:50:00');
INSERT INTO `sys_logininfor` VALUES (33, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 18:44:54');
INSERT INTO `sys_logininfor` VALUES (34, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2020-12-28 22:43:25');
INSERT INTO `sys_logininfor` VALUES (35, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-28 22:43:29');
INSERT INTO `sys_logininfor` VALUES (36, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 22:43:32');
INSERT INTO `sys_logininfor` VALUES (37, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-28 23:21:11');
INSERT INTO `sys_logininfor` VALUES (38, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 11:10:39');
INSERT INTO `sys_logininfor` VALUES (39, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 14:35:01');
INSERT INTO `sys_logininfor` VALUES (40, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 15:47:01');
INSERT INTO `sys_logininfor` VALUES (41, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-29 18:33:18');
INSERT INTO `sys_logininfor` VALUES (42, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-29 18:33:21');
INSERT INTO `sys_logininfor` VALUES (43, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2020-12-29 18:33:25');
INSERT INTO `sys_logininfor` VALUES (44, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 18:33:42');
INSERT INTO `sys_logininfor` VALUES (45, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 19:31:00');
INSERT INTO `sys_logininfor` VALUES (46, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 22:09:51');
INSERT INTO `sys_logininfor` VALUES (47, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2020-12-29 22:10:16');
INSERT INTO `sys_logininfor` VALUES (48, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2020-12-31 17:34:00');
INSERT INTO `sys_logininfor` VALUES (49, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '验证码已失效', '2020-12-31 17:37:37');
INSERT INTO `sys_logininfor` VALUES (50, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '验证码已失效', '2020-12-31 17:40:13');
INSERT INTO `sys_logininfor` VALUES (51, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '验证码已失效', '2020-12-31 17:56:18');
INSERT INTO `sys_logininfor` VALUES (52, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '验证码已失效', '2020-12-31 17:56:26');
INSERT INTO `sys_logininfor` VALUES (53, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '验证码已失效', '2020-12-31 17:56:28');
INSERT INTO `sys_logininfor` VALUES (54, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2020-12-31 18:00:16');
INSERT INTO `sys_logininfor` VALUES (55, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2020-12-31 18:00:51');
INSERT INTO `sys_logininfor` VALUES (56, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2020-12-31 18:01:00');
INSERT INTO `sys_logininfor` VALUES (57, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2020-12-31 18:04:22');
INSERT INTO `sys_logininfor` VALUES (58, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2021-01-04 11:31:42');
INSERT INTO `sys_logininfor` VALUES (59, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-04 11:31:46');
INSERT INTO `sys_logininfor` VALUES (60, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-04 13:34:22');
INSERT INTO `sys_logininfor` VALUES (61, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-04 13:34:25');
INSERT INTO `sys_logininfor` VALUES (62, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-04 13:34:32');
INSERT INTO `sys_logininfor` VALUES (63, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-04 13:57:03');
INSERT INTO `sys_logininfor` VALUES (64, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-04 13:57:06');
INSERT INTO `sys_logininfor` VALUES (65, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-04 14:50:00');
INSERT INTO `sys_logininfor` VALUES (66, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-04 17:18:32');
INSERT INTO `sys_logininfor` VALUES (67, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-05 13:47:58');
INSERT INTO `sys_logininfor` VALUES (68, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '用户不存在/密码错误', '2021-01-05 15:45:05');
INSERT INTO `sys_logininfor` VALUES (69, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '用户不存在/密码错误', '2021-01-05 15:45:32');
INSERT INTO `sys_logininfor` VALUES (70, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 15:45:58');
INSERT INTO `sys_logininfor` VALUES (71, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 15:46:09');
INSERT INTO `sys_logininfor` VALUES (72, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 15:46:14');
INSERT INTO `sys_logininfor` VALUES (73, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-05 15:47:11');
INSERT INTO `sys_logininfor` VALUES (74, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-05 15:47:38');
INSERT INTO `sys_logininfor` VALUES (75, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-05 15:50:45');
INSERT INTO `sys_logininfor` VALUES (76, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-05 15:59:16');
INSERT INTO `sys_logininfor` VALUES (77, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '用户不存在/密码错误', '2021-01-05 15:59:59');
INSERT INTO `sys_logininfor` VALUES (78, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 16:41:59');
INSERT INTO `sys_logininfor` VALUES (79, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 16:44:46');
INSERT INTO `sys_logininfor` VALUES (80, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-05 18:18:47');
INSERT INTO `sys_logininfor` VALUES (81, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-05 18:18:50');
INSERT INTO `sys_logininfor` VALUES (82, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 18:24:50');
INSERT INTO `sys_logininfor` VALUES (83, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 18:29:34');
INSERT INTO `sys_logininfor` VALUES (84, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 18:29:40');
INSERT INTO `sys_logininfor` VALUES (85, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-05 18:29:54');
INSERT INTO `sys_logininfor` VALUES (86, 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-05 22:57:25');
INSERT INTO `sys_logininfor` VALUES (87, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2021-01-06 09:31:03');
INSERT INTO `sys_logininfor` VALUES (88, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-06 09:31:06');
INSERT INTO `sys_logininfor` VALUES (89, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-06 10:30:58');
INSERT INTO `sys_logininfor` VALUES (90, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-06 13:32:50');
INSERT INTO `sys_logininfor` VALUES (91, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-06 16:11:26');
INSERT INTO `sys_logininfor` VALUES (92, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 09:46:59');
INSERT INTO `sys_logininfor` VALUES (93, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 09:47:06');
INSERT INTO `sys_logininfor` VALUES (94, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 10:56:48');
INSERT INTO `sys_logininfor` VALUES (95, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 12:33:29');
INSERT INTO `sys_logininfor` VALUES (96, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 14:26:28');
INSERT INTO `sys_logininfor` VALUES (97, 'admin', '192.168.1.114', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-01-07 17:21:00');
INSERT INTO `sys_logininfor` VALUES (98, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:27:58');
INSERT INTO `sys_logininfor` VALUES (99, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:28:08');
INSERT INTO `sys_logininfor` VALUES (100, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:28:20');
INSERT INTO `sys_logininfor` VALUES (101, 'wenfaliang', '192.168.1.117', '内网IP', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-07 18:39:37');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-07 18:40:01');
INSERT INTO `sys_logininfor` VALUES (103, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:40:49');
INSERT INTO `sys_logininfor` VALUES (104, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:41:15');
INSERT INTO `sys_logininfor` VALUES (105, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:41:26');
INSERT INTO `sys_logininfor` VALUES (106, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:42:35');
INSERT INTO `sys_logininfor` VALUES (107, NULL, '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-07 18:42:45');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-07 18:43:05');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-07 22:15:15');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 09:06:50');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 10:31:50');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-01-08 15:22:03');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-01-08 15:22:12');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-01-08 15:22:20');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-08 15:22:38');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-01-08 15:22:41');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:22:55');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:23:03');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:24:19');
INSERT INTO `sys_logininfor` VALUES (120, 'ry', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:24:29');
INSERT INTO `sys_logininfor` VALUES (121, 'ry', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:24:33');
INSERT INTO `sys_logininfor` VALUES (122, 'ry', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:24:40');
INSERT INTO `sys_logininfor` VALUES (123, 'ry', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-08 15:24:47');
INSERT INTO `sys_logininfor` VALUES (124, 'ry', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 15:24:51');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 15:25:03');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-08 15:27:44');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-08 15:27:49');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 15:57:58');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-08 16:23:08');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-08 16:23:12');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 17:12:30');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-08 17:22:20');
INSERT INTO `sys_logininfor` VALUES (133, '温发良', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-01-08 18:01:37');
INSERT INTO `sys_logininfor` VALUES (134, 'wenfl', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-08 18:01:42');
INSERT INTO `sys_logininfor` VALUES (135, 'wenfl', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-08 18:01:47');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-08 22:39:20');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-08 23:30:37');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-09 18:28:59');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '120.85.0.128', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-09 18:45:21');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '113.65.4.186', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-09 21:16:47');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2021-01-09 21:25:25');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-09 21:25:29');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-09 21:25:35');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '120.85.0.128', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-09 21:55:24');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-09 21:58:30');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-09 21:58:35');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '120.85.0.128', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-09 22:39:58');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-09 22:58:23');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-09 22:58:26');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-11 16:21:41');
INSERT INTO `sys_logininfor` VALUES (151, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-11 16:29:22');
INSERT INTO `sys_logininfor` VALUES (152, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:45:48');
INSERT INTO `sys_logininfor` VALUES (153, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:45:51');
INSERT INTO `sys_logininfor` VALUES (154, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:46:05');
INSERT INTO `sys_logininfor` VALUES (155, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:48:45');
INSERT INTO `sys_logininfor` VALUES (156, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:48:48');
INSERT INTO `sys_logininfor` VALUES (157, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:49:19');
INSERT INTO `sys_logininfor` VALUES (158, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:49:22');
INSERT INTO `sys_logininfor` VALUES (159, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:49:24');
INSERT INTO `sys_logininfor` VALUES (160, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:49:27');
INSERT INTO `sys_logininfor` VALUES (161, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:51:01');
INSERT INTO `sys_logininfor` VALUES (162, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:51:02');
INSERT INTO `sys_logininfor` VALUES (163, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:51:02');
INSERT INTO `sys_logininfor` VALUES (164, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:51:10');
INSERT INTO `sys_logininfor` VALUES (165, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:52:58');
INSERT INTO `sys_logininfor` VALUES (166, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:52:59');
INSERT INTO `sys_logininfor` VALUES (167, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:00');
INSERT INTO `sys_logininfor` VALUES (168, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:00');
INSERT INTO `sys_logininfor` VALUES (169, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:01');
INSERT INTO `sys_logininfor` VALUES (170, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:01');
INSERT INTO `sys_logininfor` VALUES (171, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:02');
INSERT INTO `sys_logininfor` VALUES (172, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:02');
INSERT INTO `sys_logininfor` VALUES (173, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:02');
INSERT INTO `sys_logininfor` VALUES (174, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (175, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (176, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (177, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (178, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (179, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:03');
INSERT INTO `sys_logininfor` VALUES (180, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:04');
INSERT INTO `sys_logininfor` VALUES (181, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:14');
INSERT INTO `sys_logininfor` VALUES (182, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:53:25');
INSERT INTO `sys_logininfor` VALUES (183, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:54:03');
INSERT INTO `sys_logininfor` VALUES (184, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:54:39');
INSERT INTO `sys_logininfor` VALUES (185, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:54:41');
INSERT INTO `sys_logininfor` VALUES (186, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:54:41');
INSERT INTO `sys_logininfor` VALUES (187, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:54:54');
INSERT INTO `sys_logininfor` VALUES (188, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:20');
INSERT INTO `sys_logininfor` VALUES (189, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:21');
INSERT INTO `sys_logininfor` VALUES (190, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:32');
INSERT INTO `sys_logininfor` VALUES (191, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:35');
INSERT INTO `sys_logininfor` VALUES (192, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:38');
INSERT INTO `sys_logininfor` VALUES (193, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:57:44');
INSERT INTO `sys_logininfor` VALUES (194, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:58:39');
INSERT INTO `sys_logininfor` VALUES (195, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 16:58:42');
INSERT INTO `sys_logininfor` VALUES (196, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:00:27');
INSERT INTO `sys_logininfor` VALUES (197, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:01:14');
INSERT INTO `sys_logininfor` VALUES (198, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:01:17');
INSERT INTO `sys_logininfor` VALUES (199, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:02:32');
INSERT INTO `sys_logininfor` VALUES (200, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:02:36');
INSERT INTO `sys_logininfor` VALUES (201, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:02:39');
INSERT INTO `sys_logininfor` VALUES (202, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:14');
INSERT INTO `sys_logininfor` VALUES (203, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:28');
INSERT INTO `sys_logininfor` VALUES (204, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:31');
INSERT INTO `sys_logininfor` VALUES (205, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:46');
INSERT INTO `sys_logininfor` VALUES (206, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:49');
INSERT INTO `sys_logininfor` VALUES (207, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:52');
INSERT INTO `sys_logininfor` VALUES (208, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:03:55');
INSERT INTO `sys_logininfor` VALUES (209, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:04:03');
INSERT INTO `sys_logininfor` VALUES (210, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:04:07');
INSERT INTO `sys_logininfor` VALUES (211, '', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 17:04:10');
INSERT INTO `sys_logininfor` VALUES (212, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:05:40');
INSERT INTO `sys_logininfor` VALUES (213, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:06:00');
INSERT INTO `sys_logininfor` VALUES (214, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:14');
INSERT INTO `sys_logininfor` VALUES (215, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:17');
INSERT INTO `sys_logininfor` VALUES (216, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:20');
INSERT INTO `sys_logininfor` VALUES (217, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:23');
INSERT INTO `sys_logininfor` VALUES (218, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:26');
INSERT INTO `sys_logininfor` VALUES (219, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:30');
INSERT INTO `sys_logininfor` VALUES (220, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:32');
INSERT INTO `sys_logininfor` VALUES (221, '123', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:09:35');
INSERT INTO `sys_logininfor` VALUES (222, '666', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:11:05');
INSERT INTO `sys_logininfor` VALUES (223, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:12:08');
INSERT INTO `sys_logininfor` VALUES (224, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:12:10');
INSERT INTO `sys_logininfor` VALUES (225, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:09');
INSERT INTO `sys_logininfor` VALUES (226, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:12');
INSERT INTO `sys_logininfor` VALUES (227, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:32');
INSERT INTO `sys_logininfor` VALUES (228, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:35');
INSERT INTO `sys_logininfor` VALUES (229, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:38');
INSERT INTO `sys_logininfor` VALUES (230, '111', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:14:40');
INSERT INTO `sys_logininfor` VALUES (231, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:15:01');
INSERT INTO `sys_logininfor` VALUES (232, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:15:04');
INSERT INTO `sys_logininfor` VALUES (233, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:16:56');
INSERT INTO `sys_logininfor` VALUES (234, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-11 18:16:59');
INSERT INTO `sys_logininfor` VALUES (235, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:26:25');
INSERT INTO `sys_logininfor` VALUES (236, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:26:27');
INSERT INTO `sys_logininfor` VALUES (237, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:26:50');
INSERT INTO `sys_logininfor` VALUES (238, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:06');
INSERT INTO `sys_logininfor` VALUES (239, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:08');
INSERT INTO `sys_logininfor` VALUES (240, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:11');
INSERT INTO `sys_logininfor` VALUES (241, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:13');
INSERT INTO `sys_logininfor` VALUES (242, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:15');
INSERT INTO `sys_logininfor` VALUES (243, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:17');
INSERT INTO `sys_logininfor` VALUES (244, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:27:20');
INSERT INTO `sys_logininfor` VALUES (245, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:30:05');
INSERT INTO `sys_logininfor` VALUES (246, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:30:23');
INSERT INTO `sys_logininfor` VALUES (247, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:31:03');
INSERT INTO `sys_logininfor` VALUES (248, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:31:35');
INSERT INTO `sys_logininfor` VALUES (249, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:31:49');
INSERT INTO `sys_logininfor` VALUES (250, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:05');
INSERT INTO `sys_logininfor` VALUES (251, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:06');
INSERT INTO `sys_logininfor` VALUES (252, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:06');
INSERT INTO `sys_logininfor` VALUES (253, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:06');
INSERT INTO `sys_logininfor` VALUES (254, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:07');
INSERT INTO `sys_logininfor` VALUES (255, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:07');
INSERT INTO `sys_logininfor` VALUES (256, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:07');
INSERT INTO `sys_logininfor` VALUES (257, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:07');
INSERT INTO `sys_logininfor` VALUES (258, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:11');
INSERT INTO `sys_logininfor` VALUES (259, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:11');
INSERT INTO `sys_logininfor` VALUES (260, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:11');
INSERT INTO `sys_logininfor` VALUES (261, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:11');
INSERT INTO `sys_logininfor` VALUES (262, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:11');
INSERT INTO `sys_logininfor` VALUES (263, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:12');
INSERT INTO `sys_logininfor` VALUES (264, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:32:12');
INSERT INTO `sys_logininfor` VALUES (265, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:33:36');
INSERT INTO `sys_logininfor` VALUES (266, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:33:39');
INSERT INTO `sys_logininfor` VALUES (267, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:44:27');
INSERT INTO `sys_logininfor` VALUES (268, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:45:32');
INSERT INTO `sys_logininfor` VALUES (269, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:12');
INSERT INTO `sys_logininfor` VALUES (270, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:14');
INSERT INTO `sys_logininfor` VALUES (271, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:16');
INSERT INTO `sys_logininfor` VALUES (272, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:28');
INSERT INTO `sys_logininfor` VALUES (273, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:33');
INSERT INTO `sys_logininfor` VALUES (274, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:34');
INSERT INTO `sys_logininfor` VALUES (275, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:35');
INSERT INTO `sys_logininfor` VALUES (276, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:35');
INSERT INTO `sys_logininfor` VALUES (277, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:35');
INSERT INTO `sys_logininfor` VALUES (278, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:35');
INSERT INTO `sys_logininfor` VALUES (279, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:35');
INSERT INTO `sys_logininfor` VALUES (280, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:36');
INSERT INTO `sys_logininfor` VALUES (281, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:36');
INSERT INTO `sys_logininfor` VALUES (282, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 18:54:38');
INSERT INTO `sys_logininfor` VALUES (283, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 19:00:51');
INSERT INTO `sys_logininfor` VALUES (284, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-11 19:05:12');
INSERT INTO `sys_logininfor` VALUES (285, 'admin', '116.23.96.176', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-11 19:09:26');
INSERT INTO `sys_logininfor` VALUES (286, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-12 09:41:26');
INSERT INTO `sys_logininfor` VALUES (287, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 11:01:05');
INSERT INTO `sys_logininfor` VALUES (288, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 11:05:02');
INSERT INTO `sys_logininfor` VALUES (289, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 11:06:19');
INSERT INTO `sys_logininfor` VALUES (290, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 11:07:26');
INSERT INTO `sys_logininfor` VALUES (291, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 11:14:50');
INSERT INTO `sys_logininfor` VALUES (292, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 11:20:06');
INSERT INTO `sys_logininfor` VALUES (293, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 13:42:55');
INSERT INTO `sys_logininfor` VALUES (294, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-12 13:54:58');
INSERT INTO `sys_logininfor` VALUES (295, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-12 13:55:01');
INSERT INTO `sys_logininfor` VALUES (296, '11', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '1', '用户不存在/密码错误', '2021-01-12 14:47:26');
INSERT INTO `sys_logininfor` VALUES (297, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 14:53:17');
INSERT INTO `sys_logininfor` VALUES (298, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 15:01:48');
INSERT INTO `sys_logininfor` VALUES (299, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 15:01:55');
INSERT INTO `sys_logininfor` VALUES (300, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 15:02:02');
INSERT INTO `sys_logininfor` VALUES (301, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 15:02:32');
INSERT INTO `sys_logininfor` VALUES (302, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 15:04:33');
INSERT INTO `sys_logininfor` VALUES (303, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-12 16:47:23');
INSERT INTO `sys_logininfor` VALUES (304, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 16:50:40');
INSERT INTO `sys_logininfor` VALUES (305, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 16:53:16');
INSERT INTO `sys_logininfor` VALUES (306, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:10:27');
INSERT INTO `sys_logininfor` VALUES (307, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:10:31');
INSERT INTO `sys_logininfor` VALUES (308, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:10:34');
INSERT INTO `sys_logininfor` VALUES (309, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:10:43');
INSERT INTO `sys_logininfor` VALUES (310, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:11:58');
INSERT INTO `sys_logininfor` VALUES (311, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:12:05');
INSERT INTO `sys_logininfor` VALUES (312, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:12:48');
INSERT INTO `sys_logininfor` VALUES (313, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:12:55');
INSERT INTO `sys_logininfor` VALUES (314, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:13:31');
INSERT INTO `sys_logininfor` VALUES (315, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:13:39');
INSERT INTO `sys_logininfor` VALUES (316, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:15:07');
INSERT INTO `sys_logininfor` VALUES (317, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:15:10');
INSERT INTO `sys_logininfor` VALUES (318, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:24:08');
INSERT INTO `sys_logininfor` VALUES (319, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:24:16');
INSERT INTO `sys_logininfor` VALUES (320, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:25:30');
INSERT INTO `sys_logininfor` VALUES (321, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:25:36');
INSERT INTO `sys_logininfor` VALUES (322, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:25:44');
INSERT INTO `sys_logininfor` VALUES (323, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:29:50');
INSERT INTO `sys_logininfor` VALUES (324, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:31:28');
INSERT INTO `sys_logininfor` VALUES (325, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:31:40');
INSERT INTO `sys_logininfor` VALUES (326, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:32:06');
INSERT INTO `sys_logininfor` VALUES (327, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:34:24');
INSERT INTO `sys_logininfor` VALUES (328, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:36:14');
INSERT INTO `sys_logininfor` VALUES (329, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 17:37:08');
INSERT INTO `sys_logininfor` VALUES (330, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:39:18');
INSERT INTO `sys_logininfor` VALUES (331, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:39:19');
INSERT INTO `sys_logininfor` VALUES (332, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:39:20');
INSERT INTO `sys_logininfor` VALUES (333, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:39:21');
INSERT INTO `sys_logininfor` VALUES (334, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:39:22');
INSERT INTO `sys_logininfor` VALUES (335, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:42:10');
INSERT INTO `sys_logininfor` VALUES (336, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:42:11');
INSERT INTO `sys_logininfor` VALUES (337, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 17:42:11');
INSERT INTO `sys_logininfor` VALUES (338, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-12 18:10:46');
INSERT INTO `sys_logininfor` VALUES (339, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 18:35:14');
INSERT INTO `sys_logininfor` VALUES (340, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-12 18:38:47');
INSERT INTO `sys_logininfor` VALUES (341, 'admin', '116.23.96.176', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-12 19:06:23');
INSERT INTO `sys_logininfor` VALUES (342, 'admin', '116.23.99.31', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 10:10:05');
INSERT INTO `sys_logininfor` VALUES (343, 'admin', '116.23.99.31', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-13 10:12:14');
INSERT INTO `sys_logininfor` VALUES (344, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 13:32:04');
INSERT INTO `sys_logininfor` VALUES (345, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 13:32:27');
INSERT INTO `sys_logininfor` VALUES (346, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 14:07:19');
INSERT INTO `sys_logininfor` VALUES (347, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 14:10:41');
INSERT INTO `sys_logininfor` VALUES (348, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 14:10:45');
INSERT INTO `sys_logininfor` VALUES (349, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 15:35:14');
INSERT INTO `sys_logininfor` VALUES (350, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 16:16:33');
INSERT INTO `sys_logininfor` VALUES (351, 'admin', '192.168.1.196', '内网IP', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 16:42:10');
INSERT INTO `sys_logininfor` VALUES (352, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 16:47:07');
INSERT INTO `sys_logininfor` VALUES (353, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 16:47:48');
INSERT INTO `sys_logininfor` VALUES (354, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 16:50:18');
INSERT INTO `sys_logininfor` VALUES (355, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 16:50:57');
INSERT INTO `sys_logininfor` VALUES (356, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 16:53:56');
INSERT INTO `sys_logininfor` VALUES (357, 'admin', '192.168.4.11', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-13 16:57:24');
INSERT INTO `sys_logininfor` VALUES (358, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:09:16');
INSERT INTO `sys_logininfor` VALUES (359, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:09:32');
INSERT INTO `sys_logininfor` VALUES (360, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:11:52');
INSERT INTO `sys_logininfor` VALUES (361, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:11:56');
INSERT INTO `sys_logininfor` VALUES (362, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:15');
INSERT INTO `sys_logininfor` VALUES (363, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:37');
INSERT INTO `sys_logininfor` VALUES (364, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:42');
INSERT INTO `sys_logininfor` VALUES (365, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:45');
INSERT INTO `sys_logininfor` VALUES (366, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:48');
INSERT INTO `sys_logininfor` VALUES (367, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:50');
INSERT INTO `sys_logininfor` VALUES (368, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:19:51');
INSERT INTO `sys_logininfor` VALUES (369, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:24:48');
INSERT INTO `sys_logininfor` VALUES (370, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:25:00');
INSERT INTO `sys_logininfor` VALUES (371, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:26:52');
INSERT INTO `sys_logininfor` VALUES (372, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:26:58');
INSERT INTO `sys_logininfor` VALUES (373, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:27:27');
INSERT INTO `sys_logininfor` VALUES (374, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:28:44');
INSERT INTO `sys_logininfor` VALUES (375, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:28:48');
INSERT INTO `sys_logininfor` VALUES (376, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:33:38');
INSERT INTO `sys_logininfor` VALUES (377, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:35:06');
INSERT INTO `sys_logininfor` VALUES (378, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:35:10');
INSERT INTO `sys_logininfor` VALUES (379, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:35:58');
INSERT INTO `sys_logininfor` VALUES (380, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:36:10');
INSERT INTO `sys_logininfor` VALUES (381, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:36:26');
INSERT INTO `sys_logininfor` VALUES (382, 'admin', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:36:29');
INSERT INTO `sys_logininfor` VALUES (383, 'admin', '192.168.4.11', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-13 17:40:19');
INSERT INTO `sys_logininfor` VALUES (384, 'admin', '192.168.4.11', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-13 17:40:23');
INSERT INTO `sys_logininfor` VALUES (385, 'wenfl', '192.168.4.11', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-13 17:59:32');
INSERT INTO `sys_logininfor` VALUES (386, 'wenfl', '120.85.8.55', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-13 22:15:11');
INSERT INTO `sys_logininfor` VALUES (387, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-13 22:20:34');
INSERT INTO `sys_logininfor` VALUES (388, 'admin', '120.85.8.55', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-13 22:33:48');
INSERT INTO `sys_logininfor` VALUES (389, 'admin', '116.23.97.98', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-14 09:04:24');
INSERT INTO `sys_logininfor` VALUES (390, 'admin', '116.23.97.98', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-14 10:14:01');
INSERT INTO `sys_logininfor` VALUES (391, 'admin', '116.23.97.98', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-14 10:51:14');
INSERT INTO `sys_logininfor` VALUES (392, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 11:21:02');
INSERT INTO `sys_logininfor` VALUES (393, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 14:29:59');
INSERT INTO `sys_logininfor` VALUES (394, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 14:44:24');
INSERT INTO `sys_logininfor` VALUES (395, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 16:05:55');
INSERT INTO `sys_logininfor` VALUES (396, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 16:09:52');
INSERT INTO `sys_logininfor` VALUES (397, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 17:54:21');
INSERT INTO `sys_logininfor` VALUES (398, 'jiangl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 17:55:37');
INSERT INTO `sys_logininfor` VALUES (399, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 18:43:30');
INSERT INTO `sys_logininfor` VALUES (400, 'jiangl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 18:49:51');
INSERT INTO `sys_logininfor` VALUES (401, 'jiangl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-14 18:50:12');
INSERT INTO `sys_logininfor` VALUES (402, 'wenfl', '120.85.0.6', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 21:51:25');
INSERT INTO `sys_logininfor` VALUES (403, 'wenfl', '120.85.0.6', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-14 21:51:59');
INSERT INTO `sys_logininfor` VALUES (404, 'jiangl', '120.85.0.6', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-14 22:06:04');
INSERT INTO `sys_logininfor` VALUES (405, 'wubt', '120.85.0.6', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-14 22:11:09');
INSERT INTO `sys_logininfor` VALUES (406, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 09:54:30');
INSERT INTO `sys_logininfor` VALUES (407, 'wenfl', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 09:54:30');
INSERT INTO `sys_logininfor` VALUES (408, 'wubt', '116.23.97.98', 'XX XX', 'Unknown', 'Android', '0', '登录成功', '2021-01-15 10:02:05');
INSERT INTO `sys_logininfor` VALUES (409, 'wubt', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 10 (iPhone)', '0', '登录成功', '2021-01-15 10:48:18');
INSERT INTO `sys_logininfor` VALUES (410, 'wubt', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 11:11:57');
INSERT INTO `sys_logininfor` VALUES (411, 'wubt', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 11:12:09');
INSERT INTO `sys_logininfor` VALUES (412, 'wubt', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 13:32:31');
INSERT INTO `sys_logininfor` VALUES (413, 'wubt', '116.23.97.98', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 13:32:35');
INSERT INTO `sys_logininfor` VALUES (414, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-15 14:09:16');
INSERT INTO `sys_logininfor` VALUES (415, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 15:32:01');
INSERT INTO `sys_logininfor` VALUES (416, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 15:32:07');
INSERT INTO `sys_logininfor` VALUES (417, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-15 15:38:42');
INSERT INTO `sys_logininfor` VALUES (418, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 16:35:41');
INSERT INTO `sys_logininfor` VALUES (419, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:05:29');
INSERT INTO `sys_logininfor` VALUES (420, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:13:13');
INSERT INTO `sys_logininfor` VALUES (421, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:13:24');
INSERT INTO `sys_logininfor` VALUES (422, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:15:59');
INSERT INTO `sys_logininfor` VALUES (423, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:17:21');
INSERT INTO `sys_logininfor` VALUES (424, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:22:06');
INSERT INTO `sys_logininfor` VALUES (425, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 17:23:47');
INSERT INTO `sys_logininfor` VALUES (426, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 18:33:33');
INSERT INTO `sys_logininfor` VALUES (427, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 20:39:36');
INSERT INTO `sys_logininfor` VALUES (428, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-15 23:55:32');
INSERT INTO `sys_logininfor` VALUES (429, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-16 11:30:27');
INSERT INTO `sys_logininfor` VALUES (430, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-16 14:05:37');
INSERT INTO `sys_logininfor` VALUES (431, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-16 22:35:14');
INSERT INTO `sys_logininfor` VALUES (432, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-16 23:25:01');
INSERT INTO `sys_logininfor` VALUES (433, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-16 23:34:59');
INSERT INTO `sys_logininfor` VALUES (434, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-16 23:58:37');
INSERT INTO `sys_logininfor` VALUES (435, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-17 00:44:42');
INSERT INTO `sys_logininfor` VALUES (436, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-17 00:44:46');
INSERT INTO `sys_logininfor` VALUES (437, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-17 01:51:48');
INSERT INTO `sys_logininfor` VALUES (438, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-17 22:31:29');
INSERT INTO `sys_logininfor` VALUES (439, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-18 09:43:51');
INSERT INTO `sys_logininfor` VALUES (440, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-18 09:43:56');
INSERT INTO `sys_logininfor` VALUES (441, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 10:20:47');
INSERT INTO `sys_logininfor` VALUES (442, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 11:33:46');
INSERT INTO `sys_logininfor` VALUES (443, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 12:42:52');
INSERT INTO `sys_logininfor` VALUES (444, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 13:53:34');
INSERT INTO `sys_logininfor` VALUES (445, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 14:04:37');
INSERT INTO `sys_logininfor` VALUES (446, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 14:51:44');
INSERT INTO `sys_logininfor` VALUES (447, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-18 15:05:06');
INSERT INTO `sys_logininfor` VALUES (448, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-18 15:05:09');
INSERT INTO `sys_logininfor` VALUES (449, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-18 15:05:14');
INSERT INTO `sys_logininfor` VALUES (450, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 17:37:29');
INSERT INTO `sys_logininfor` VALUES (451, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-18 23:08:50');
INSERT INTO `sys_logininfor` VALUES (452, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-19 09:25:43');
INSERT INTO `sys_logininfor` VALUES (453, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2021-01-19 11:08:21');
INSERT INTO `sys_logininfor` VALUES (454, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 11:10:02');
INSERT INTO `sys_logininfor` VALUES (455, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 11:41:48');
INSERT INTO `sys_logininfor` VALUES (456, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-19 12:30:08');
INSERT INTO `sys_logininfor` VALUES (457, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 12:30:13');
INSERT INTO `sys_logininfor` VALUES (458, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-01-19 13:36:55');
INSERT INTO `sys_logininfor` VALUES (459, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 13:37:00');
INSERT INTO `sys_logininfor` VALUES (460, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 15:26:41');
INSERT INTO `sys_logininfor` VALUES (461, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 18:18:12');
INSERT INTO `sys_logininfor` VALUES (462, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-19 18:46:44');
INSERT INTO `sys_logininfor` VALUES (463, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-19 22:05:17');
INSERT INTO `sys_logininfor` VALUES (464, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-19 22:41:20');
INSERT INTO `sys_logininfor` VALUES (465, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-19 22:47:27');
INSERT INTO `sys_logininfor` VALUES (466, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-19 22:50:53');
INSERT INTO `sys_logininfor` VALUES (467, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 09:59:56');
INSERT INTO `sys_logininfor` VALUES (468, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 10:02:51');
INSERT INTO `sys_logininfor` VALUES (469, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 10:05:23');
INSERT INTO `sys_logininfor` VALUES (470, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 12:46:16');
INSERT INTO `sys_logininfor` VALUES (471, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 15:57:56');
INSERT INTO `sys_logininfor` VALUES (472, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 17:04:35');
INSERT INTO `sys_logininfor` VALUES (473, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-20 17:11:57');
INSERT INTO `sys_logininfor` VALUES (474, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 17:12:35');
INSERT INTO `sys_logininfor` VALUES (475, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-20 17:44:02');
INSERT INTO `sys_logininfor` VALUES (476, 'admin', '116.23.97.98', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-21 14:46:42');
INSERT INTO `sys_logininfor` VALUES (477, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-21 15:52:24');
INSERT INTO `sys_logininfor` VALUES (478, 'admin', '116.23.97.98', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-21 15:53:19');
INSERT INTO `sys_logininfor` VALUES (479, 'wudj', '116.23.98.224', 'XX XX', 'Unknown', 'Unknown', '1', '用户不存在/密码错误', '2021-01-21 17:38:31');
INSERT INTO `sys_logininfor` VALUES (480, 'wubt', '116.23.98.224', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-21 17:38:37');
INSERT INTO `sys_logininfor` VALUES (481, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 18:02:46');
INSERT INTO `sys_logininfor` VALUES (482, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 18:56:16');
INSERT INTO `sys_logininfor` VALUES (483, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 18:57:23');
INSERT INTO `sys_logininfor` VALUES (484, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 18:58:52');
INSERT INTO `sys_logininfor` VALUES (485, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 19:00:46');
INSERT INTO `sys_logininfor` VALUES (486, 'wubt', '192.168.4.11', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-21 19:02:35');
INSERT INTO `sys_logininfor` VALUES (487, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-22 10:25:33');
INSERT INTO `sys_logininfor` VALUES (488, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-22 11:03:14');
INSERT INTO `sys_logininfor` VALUES (489, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-22 13:41:03');
INSERT INTO `sys_logininfor` VALUES (490, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-22 15:52:53');
INSERT INTO `sys_logininfor` VALUES (491, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-22 23:31:08');
INSERT INTO `sys_logininfor` VALUES (492, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-23 11:00:14');
INSERT INTO `sys_logininfor` VALUES (493, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-23 11:01:33');
INSERT INTO `sys_logininfor` VALUES (494, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-23 11:02:34');
INSERT INTO `sys_logininfor` VALUES (495, 'wubt', '116.23.98.224', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-23 11:04:58');
INSERT INTO `sys_logininfor` VALUES (496, 'wubt', '127.0.0.1', '内网IP', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-23 11:05:21');
INSERT INTO `sys_logininfor` VALUES (497, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-23 13:10:35');
INSERT INTO `sys_logininfor` VALUES (498, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-23 13:50:41');
INSERT INTO `sys_logininfor` VALUES (499, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-23 15:10:10');
INSERT INTO `sys_logininfor` VALUES (500, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-25 11:49:48');
INSERT INTO `sys_logininfor` VALUES (501, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-25 14:34:35');
INSERT INTO `sys_logininfor` VALUES (502, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-25 16:38:43');
INSERT INTO `sys_logininfor` VALUES (503, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-25 17:39:24');
INSERT INTO `sys_logininfor` VALUES (504, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-26 09:21:22');
INSERT INTO `sys_logininfor` VALUES (505, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-26 15:19:26');
INSERT INTO `sys_logininfor` VALUES (506, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-26 16:07:14');
INSERT INTO `sys_logininfor` VALUES (507, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-26 16:45:14');
INSERT INTO `sys_logininfor` VALUES (508, 'jiangl', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-26 16:52:08');
INSERT INTO `sys_logininfor` VALUES (509, 'wubt', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-26 18:15:22');
INSERT INTO `sys_logininfor` VALUES (510, 'wubt', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-26 18:15:23');
INSERT INTO `sys_logininfor` VALUES (511, 'wubt', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '1', '用户不存在/密码错误', '2021-01-26 19:03:15');
INSERT INTO `sys_logininfor` VALUES (512, 'wubt', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-26 19:06:35');
INSERT INTO `sys_logininfor` VALUES (513, 'wubt', '192.168.5.223', '内网IP', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-26 19:18:03');
INSERT INTO `sys_logininfor` VALUES (514, 'wubt', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-27 17:26:43');
INSERT INTO `sys_logininfor` VALUES (515, 'wubt', '116.23.97.237', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-28 17:10:46');
INSERT INTO `sys_logininfor` VALUES (516, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-01-28 17:25:33');
INSERT INTO `sys_logininfor` VALUES (517, 'wubt', '116.23.97.237', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-28 18:14:54');
INSERT INTO `sys_logininfor` VALUES (518, 'wubt', '116.23.97.237', 'XX XX', 'Unknown', 'Unknown', '0', '登录成功', '2021-01-28 18:53:59');
INSERT INTO `sys_logininfor` VALUES (519, 'wubt', '116.23.97.237', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-28 19:07:25');
INSERT INTO `sys_logininfor` VALUES (520, 'wubt', '129.211.76.166', 'XX XX', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2021-01-28 21:26:44');
INSERT INTO `sys_logininfor` VALUES (521, 'wubt', '113.115.59.134', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-28 21:51:58');
INSERT INTO `sys_logininfor` VALUES (522, 'wenfl', '223.104.64.241', 'XX XX', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-01-28 21:54:33');
INSERT INTO `sys_logininfor` VALUES (523, 'jiangl', '116.23.98.239', 'XX XX', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-01-28 21:54:58');
INSERT INTO `sys_logininfor` VALUES (524, 'chenyc', '116.22.160.182', 'XX XX', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-01-28 22:01:02');
INSERT INTO `sys_logininfor` VALUES (525, 'dingw', '120.85.0.156', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-28 22:24:53');
INSERT INTO `sys_logininfor` VALUES (526, 'chenyc', '116.22.160.182', 'XX XX', 'Chrome Mobile', 'Android 1.x', '0', '登录成功', '2021-01-28 22:56:44');
INSERT INTO `sys_logininfor` VALUES (527, 'jiangl', '61.140.94.7', 'XX XX', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2021-01-28 23:06:10');
INSERT INTO `sys_logininfor` VALUES (528, 'dingw', '113.115.59.134', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-01-29 12:15:54');
INSERT INTO `sys_logininfor` VALUES (529, 'dingw', '116.23.97.237', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-02-01 09:34:13');
INSERT INTO `sys_logininfor` VALUES (530, 'wubt', '116.23.97.237', 'XX XX', 'Chrome 53', 'Windows 7', '0', '登录成功', '2021-02-01 14:34:09');
INSERT INTO `sys_logininfor` VALUES (531, 'dingw', '116.23.97.237', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-02-01 18:51:55');
INSERT INTO `sys_logininfor` VALUES (532, 'wubt', '116.23.97.237', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-02-01 18:53:52');
INSERT INTO `sys_logininfor` VALUES (533, 'admin', '116.23.97.237', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-02-01 18:54:54');
INSERT INTO `sys_logininfor` VALUES (534, 'wubt', '60.236.184.102', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-02-01 20:25:12');
INSERT INTO `sys_logininfor` VALUES (535, 'wubt', '116.23.97.43', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2021-03-17 19:37:13');
INSERT INTO `sys_logininfor` VALUES (536, 'admin', '116.23.96.226', 'XX XX', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-05-24 14:55:26');
INSERT INTO `sys_logininfor` VALUES (537, 'wubt', '116.23.96.226', 'XX XX', 'Chrome 53', 'Windows 7', '0', '登录成功', '2021-05-24 15:02:51');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件路径',
  `is_frame` int(1) DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1099 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2020-12-21 17:51:28', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2020-12-21 17:51:28', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2020-12-21 17:51:28', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2020-12-21 17:51:28', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2020-12-21 17:51:28', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2020-12-21 17:51:28', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2020-12-21 17:51:28', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2020-12-21 17:51:28', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2020-12-21 17:51:28', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2020-12-21 17:51:28', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2020-12-21 17:51:28', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2020-12-21 17:51:28', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2020-12-21 17:51:28', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2020-12-21 17:51:28', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2020-12-21 17:51:28', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2020-12-21 17:51:28', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2020-12-21 17:51:28', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2020-12-21 17:51:28', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2020-12-21 17:51:28', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2020-12-21 17:51:28', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2020-12-21 17:51:28', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2020-12-21 17:51:28', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2020-12-21 17:51:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '业务系统', 0, 4, 'business', NULL, 1, 0, 'M', '0', '0', '', 'people', 'admin', '2020-12-23 15:22:56', 'admin', '2020-12-23 15:24:04', '');
INSERT INTO `sys_menu` VALUES (1062, '意见', 1061, 1, 'suggest', 'fb/suggest/index', 1, 0, 'C', '0', '0', 'fb:suggest:list', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '意见菜单');
INSERT INTO `sys_menu` VALUES (1063, '意见查询', 1062, 1, '#', '', 1, 0, 'F', '0', '0', 'fb:suggest:query', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1064, '意见新增', 1062, 2, '#', '', 1, 0, 'F', '0', '0', 'fb:suggest:add', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1065, '意见修改', 1062, 3, '#', '', 1, 0, 'F', '0', '0', 'fb:suggest:edit', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1066, '意见删除', 1062, 4, '#', '', 1, 0, 'F', '0', '0', 'fb:suggest:remove', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1067, '意见导出', 1062, 5, '#', '', 1, 0, 'F', '0', '0', 'fb:suggest:export', '#', 'admin', '2020-12-23 16:00:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1068, '食堂', 1061, 1, 'canteen', 'fb/canteen/index', 1, 0, 'C', '0', '0', 'fb:canteen:list', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '食堂菜单');
INSERT INTO `sys_menu` VALUES (1069, '食堂查询', 1068, 1, '#', '', 1, 0, 'F', '0', '0', 'fb:canteen:query', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1070, '食堂新增', 1068, 2, '#', '', 1, 0, 'F', '0', '0', 'fb:canteen:add', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1071, '食堂修改', 1068, 3, '#', '', 1, 0, 'F', '0', '0', 'fb:canteen:edit', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1072, '食堂删除', 1068, 4, '#', '', 1, 0, 'F', '0', '0', 'fb:canteen:remove', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1073, '食堂导出', 1068, 5, '#', '', 1, 0, 'F', '0', '0', 'fb:canteen:export', '#', 'admin', '2020-12-23 22:00:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1074, '食堂菜单', 1061, 1, 'menu', 'fb/menu/index', 1, 0, 'C', '0', '0', 'fb:menu:list', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '食堂菜单菜单');
INSERT INTO `sys_menu` VALUES (1075, '食堂菜单查询', 1074, 1, '#', '', 1, 0, 'F', '0', '0', 'fb:menu:query', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1076, '食堂菜单新增', 1074, 2, '#', '', 1, 0, 'F', '0', '0', 'fb:menu:add', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1077, '食堂菜单修改', 1074, 3, '#', '', 1, 0, 'F', '0', '0', 'fb:menu:edit', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1078, '食堂菜单删除', 1074, 4, '#', '', 1, 0, 'F', '0', '0', 'fb:menu:remove', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1079, '食堂菜单导出', 1074, 5, '#', '', 1, 0, 'F', '0', '0', 'fb:menu:export', '#', 'admin', '2020-12-23 22:01:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1080, '文件信息', 1, 1, 'fileinfo', 'system/fileinfo/index', 1, 0, 'C', '0', '0', 'system:fileinfo:list', 'upload', 'admin', '2020-12-25 17:01:48', 'admin', '2020-12-25 17:19:36', '文件信息菜单');
INSERT INTO `sys_menu` VALUES (1081, '文件信息查询', 1080, 1, '#', '', 1, 0, 'F', '0', '0', 'system:fileinfo:query', '#', 'admin', '2020-12-25 17:01:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1082, '文件信息新增', 1080, 2, '#', '', 1, 0, 'F', '0', '0', 'system:fileinfo:add', '#', 'admin', '2020-12-25 17:01:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1083, '文件信息修改', 1080, 3, '#', '', 1, 0, 'F', '0', '0', 'system:fileinfo:edit', '#', 'admin', '2020-12-25 17:01:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1084, '文件信息删除', 1080, 4, '#', '', 1, 0, 'F', '0', '0', 'system:fileinfo:remove', '#', 'admin', '2020-12-25 17:01:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1085, '文件信息导出', 1080, 5, '#', '', 1, 0, 'F', '0', '0', 'system:fileinfo:export', '#', 'admin', '2020-12-25 17:01:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1086, '交易记录', 1061, 1, 'info', 'fb/info/index', 1, 0, 'C', '0', '0', 'fb:info:list', '#', 'admin', '2021-01-04 13:46:59', 'admin', '2021-01-04 13:58:52', '交易记录菜单');
INSERT INTO `sys_menu` VALUES (1087, '交易记录查询', 1086, 1, '#', '', 1, 0, 'F', '0', '0', 'fb:info:query', '#', 'admin', '2021-01-04 13:46:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1088, '交易记录新增', 1086, 2, '#', '', 1, 0, 'F', '0', '0', 'fb:info:add', '#', 'admin', '2021-01-04 13:46:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1089, '交易记录修改', 1086, 3, '#', '', 1, 0, 'F', '0', '0', 'fb:info:edit', '#', 'admin', '2021-01-04 13:46:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1090, '交易记录删除', 1086, 4, '#', '', 1, 0, 'F', '0', '0', 'fb:info:remove', '#', 'admin', '2021-01-04 13:46:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1091, '交易记录导出', 1086, 5, '#', '', 1, 0, 'F', '0', '0', 'fb:info:export', '#', 'admin', '2021-01-04 13:46:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1092, '余额', 1061, 1, 'wallet', 'fb/wallet/index', 1, 0, 'C', '0', '0', 'fb:wallet:list', '#', 'admin', '2021-01-04 13:47:48', 'admin', '2021-01-04 13:59:00', '余额菜单');
INSERT INTO `sys_menu` VALUES (1093, '余额查询', 1092, 1, '#', '', 1, 0, 'F', '0', '0', 'fb:wallet:query', '#', 'admin', '2021-01-04 13:47:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1094, '余额新增', 1092, 2, '#', '', 1, 0, 'F', '0', '0', 'fb:wallet:add', '#', 'admin', '2021-01-04 13:47:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1095, '余额修改', 1092, 3, '#', '', 1, 0, 'F', '0', '0', 'fb:wallet:edit', '#', 'admin', '2021-01-04 13:47:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1096, '余额删除', 1092, 4, '#', '', 1, 0, 'F', '0', '0', 'fb:wallet:remove', '#', 'admin', '2021-01-04 13:47:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1097, '余额导出', 1092, 5, '#', '', 1, 0, 'F', '0', '0', 'fb:wallet:export', '#', 'admin', '2021-01-04 13:47:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1098, '文件上传', 1, 1, 'oss', 'system/oss/oss', 1, 0, 'C', '0', '0', 'system:oss:list', 'upload', 'admin', '2021-01-19 11:32:19', 'admin', '2021-01-19 11:34:30', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `dept_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC9A323031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (2, '维护通知', '1', 0x3C703EE7BBB4E68AA4E9809AE79FA5EFBC9A323031382D30372D303120E88BA5E4BE9DE7B3BBE7BB9FE5878CE699A8E7BBB4E68AA43C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:31', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (3, '我是测试', '1', 0x3C703EE6B58BE8AF953C2F703E, '0', 'admin', '2021-01-08 18:36:09', 'admin', '2021-01-08 23:06:27', NULL, '103,108');
INSERT INTO `sys_notice` VALUES (4, '更新通知', '1', 0x3C703EE59084E4BD8DE8AFB7E6B3A8E6848FEFBC8CE68891E9A9ACE4B88AE8A681E69BB4E696B0E4BA863C2F703E, '0', 'admin', '2021-01-08 23:08:48', 'admin', '2021-01-17 00:45:43', NULL, '100,101,103');
INSERT INTO `sys_notice` VALUES (5, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (7, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (8, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (9, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (10, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (11, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (12, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (13, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');
INSERT INTO `sys_notice` VALUES (14, '版本更新', '2', 0x3C703EE6B8A9E9A6A8E68F90E98692EFBC3F3031382D30372D303120E88BA5E4BE9DE696B0E78988E69CACE58F91E5B883E595A63C2F703E, '0', 'admin', '2020-12-21 17:51:28', 'admin', '2021-01-17 00:45:15', '管理员', '100,101,103');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 232 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_canteen,fb_canteen_menu,fb_menu,fb_suggest', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:19:01');
INSERT INTO `sys_oper_log` VALUES (2, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2020-12-23 15:21:33');
INSERT INTO `sys_oper_log` VALUES (3, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"guide\",\"orderNum\":\"4\",\"menuName\":\"若依官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"http://ruoyi.vip\",\"children\":[],\"createTime\":1608544288000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:21:41');
INSERT INTO `sys_oper_log` VALUES (4, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2020-12-23 15:21:45');
INSERT INTO `sys_oper_log` VALUES (5, '菜单管理', 1, 'com.wuhuacloud.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":\"4\",\"menuName\":\"业务系统\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"business\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:22:56');
INSERT INTO `sys_oper_log` VALUES (6, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:23:10');
INSERT INTO `sys_oper_log` VALUES (7, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:23:20');
INSERT INTO `sys_oper_log` VALUES (8, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"people\",\"orderNum\":\"4\",\"menuName\":\"业务系统\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"business\",\"children\":[],\"createTime\":1608708176000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1061,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:24:04');
INSERT INTO `sys_oper_log` VALUES (9, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"suggestId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":true,\"columnName\":\"suggest_id\"},{\"usableColumn\":false,\"columnId\":36,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":37,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":38,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"updateBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"更新者\",\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"update_by\"},{\"usableColumn\":false,\"columnId\":39,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:55:14');
INSERT INTO `sys_oper_log` VALUES (10, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-23 15:55:19');
INSERT INTO `sys_oper_log` VALUES (11, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"suggestId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1608710114000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":true,\"columnName\":\"suggest_id\"},{\"usableColumn\":false,\"columnId\":36,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"updateTime\":1608710114000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":37,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1608710114000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":38,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"updateBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"更新者\",\"updateTime\":1608710114000,\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"update_by\"},{\"u', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:55:35');
INSERT INTO `sys_oper_log` VALUES (12, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"suggestId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1608710135000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":true,\"columnName\":\"suggest_id\"},{\"usableColumn\":false,\"columnId\":36,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"updateTime\":1608710135000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":37,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1608710135000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"tableId\":4,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":38,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"updateBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"更新者\",\"updateTime\":1608710135000,\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"update_by\"},{\"u', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 15:59:13');
INSERT INTO `sys_oper_log` VALUES (13, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-23 15:59:18');
INSERT INTO `sys_oper_log` VALUES (14, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"食堂编号\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"食堂名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"canteen_name\"},{\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenTel\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"食堂电话\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"canteen_tel\"},{\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenAddr\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"食堂地址\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"canteen_addr\"},{\"usabl', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 21:58:03');
INSERT INTO `sys_oper_log` VALUES (15, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":21,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"menuId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"菜单ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":3,\"pk\":true,\"columnName\":\"menu_id\"},{\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"menuName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"menu_name\"},{\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"menuType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单类型（荤菜、素菜、组合）\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"menu_type\"},{\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"menuImg\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单图片\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 21:58:44');
INSERT INTO `sys_oper_log` VALUES (16, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-23 21:58:53');
INSERT INTO `sys_oper_log` VALUES (17, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"食堂编号\",\"updateTime\":1608731883000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"食堂名称\",\"isQuery\":\"1\",\"updateTime\":1608731883000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"canteen_name\"},{\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenTel\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"食堂电话\",\"updateTime\":1608731883000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"canteen_tel\"},{\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"canteenAddr\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"食堂地址\",\"updateTime\":1608731883000,\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"ta', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-23 22:22:31');
INSERT INTO `sys_oper_log` VALUES (18, '字典类型', 1, 'com.wuhuacloud.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"食堂菜单类型\",\"params\":{},\"dictType\":\"fb_menu_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:43:56');
INSERT INTO `sys_oper_log` VALUES (19, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"H\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_menu_type\",\"dictLabel\":\"荤菜\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:44:48');
INSERT INTO `sys_oper_log` VALUES (20, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"S\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_menu_type\",\"dictLabel\":\"素菜\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:45:00');
INSERT INTO `sys_oper_log` VALUES (21, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"S\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_menu_type\",\"dictLabel\":\"素菜\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608867900000,\"dictCode\":30,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:45:07');
INSERT INTO `sys_oper_log` VALUES (22, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"S\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_menu_type\",\"dictLabel\":\"素菜\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608867900000,\"dictCode\":30,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:45:07');
INSERT INTO `sys_oper_log` VALUES (23, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"Z\",\"dictSort\":2,\"params\":{},\"dictType\":\"fb_menu_type\",\"dictLabel\":\"组合\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 11:45:20');
INSERT INTO `sys_oper_log` VALUES (24, '用户头像', 2, 'com.wuhuacloud.web.controller.system.SysProfileController.avatar()', 'POST', 1, 'admin', NULL, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/avatar/2020/12/25/d03fe7bd-6be4-4706-b2b9-918b17ca2f7c.jpeg\",\"code\":200}', 0, NULL, '2020-12-25 12:37:20');
INSERT INTO `sys_oper_log` VALUES (25, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":21,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"menuId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"菜单ID\",\"updateTime\":1608731924000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608707941000,\"tableId\":3,\"pk\":true,\"columnName\":\"menu_id\"},{\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"menuName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单名称\",\"isQuery\":\"1\",\"updateTime\":1608731924000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(30)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"menu_name\"},{\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"fb_menu_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"menuType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单类型（荤菜、素菜、组合）\",\"isQuery\":\"1\",\"updateTime\":1608731924000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"char(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608707941000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"menu_type\"},{\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"menuImg\",\"htmlType\":\"uploadImage\",\"edit\":true,\"query\":true,\"columnComment\":\"菜单图片\",\"isQuery\":\"1\",\"updateTime\":1608731924000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(1', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 15:05:56');
INSERT INTO `sys_oper_log` VALUES (26, '食堂', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"广州食堂1\",\"canteenPrincipal\":\"你大爷\",\"canteenLatitude\":10,\"canteenAddr\":\"广州市南沙区1\",\"remark\":\"11\",\"params\":{},\"canteenSort\":1,\"canteenTel\":\"10086\",\"createTime\":1608880693355,\"id\":1,\"canteenLongitude\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 15:18:13');
INSERT INTO `sys_oper_log` VALUES (27, '食堂', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"广州食堂2\",\"canteenPrincipal\":\"aaa\",\"canteenAddr\":\"广州市海珠1\",\"params\":{},\"canteenSort\":1,\"canteenTel\":\"13800138000\",\"createTime\":1608882288581,\"id\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 15:44:48');
INSERT INTO `sys_oper_log` VALUES (28, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"menuPrice\":15,\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"红烧肉\",\"params\":{},\"createTime\":1608883280804,\"menuType\":\"H\",\"status\":\"0\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\MenuMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.MenuMapper.insertMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_menu          ( menu_name,             menu_type,             menu_img,             order_num,             menu_price,             visible,             status,                          create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\n; Field \'canteen_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value', '2020-12-25 16:01:21');
INSERT INTO `sys_oper_log` VALUES (29, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"menuPrice\":15,\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"红烧肉\",\"params\":{},\"createTime\":1608883286091,\"menuType\":\"H\",\"status\":\"0\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\MenuMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.MenuMapper.insertMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_menu          ( menu_name,             menu_type,             menu_img,             order_num,             menu_price,             visible,             status,                          create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\n; Field \'canteen_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value', '2020-12-25 16:01:26');
INSERT INTO `sys_oper_log` VALUES (30, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"menuPrice\":15,\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"红烧肉\",\"params\":{},\"createTime\":1608883290124,\"menuType\":\"H\",\"status\":\"0\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\MenuMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.MenuMapper.insertMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_menu          ( menu_name,             menu_type,             menu_img,             order_num,             menu_price,             visible,             status,                          create_time )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value\n; Field \'canteen_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'canteen_id\' doesn\'t have a default value', '2020-12-25 16:01:30');
INSERT INTO `sys_oper_log` VALUES (31, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"menuPrice\":15,\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"红烧肉\",\"params\":{},\"createTime\":1608883336662,\"menuId\":1,\"menuType\":\"H\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:02:16');
INSERT INTO `sys_oper_log` VALUES (32, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"menuPrice\":20,\"orderNum\":0,\"menuName\":\"红烧排骨\",\"params\":{},\"createTime\":1608883886370,\"menuId\":2,\"menuType\":\"H\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:11:26');
INSERT INTO `sys_oper_log` VALUES (33, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"menuPrice\":20,\"orderNum\":0,\"menuName\":\"青菜\",\"params\":{},\"createTime\":1608883903161,\"menuId\":3,\"menuType\":\"S\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:11:43');
INSERT INTO `sys_oper_log` VALUES (34, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"menuPrice\":20,\"orderNum\":0,\"menuName\":\"测试1\",\"params\":{},\"createTime\":1608883920377,\"menuId\":4,\"menuType\":\"Z\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:12:00');
INSERT INTO `sys_oper_log` VALUES (35, '食堂', 2, 'com.wuhuacloud.web.controller.fb.CanteenController.edit()', 'PUT', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"广州食堂1\",\"canteenPrincipal\":\"你大爷\",\"canteenLatitude\":10,\"canteenAddr\":\"广州市南沙区1\",\"remark\":\"11\",\"updateTime\":1608886161187,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"canteenSort\":1,\"createBy\":\"\",\"canteenTel\":\"10086\",\"createTime\":1608880693000,\"updateBy\":\"\",\"id\":1,\"canteenLongitude\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:49:21');
INSERT INTO `sys_oper_log` VALUES (36, '食堂', 2, 'com.wuhuacloud.web.controller.fb.CanteenController.edit()', 'PUT', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"广州食堂1\",\"canteenPrincipal\":\"你大爷\",\"canteenLatitude\":10,\"canteenAddr\":\"广州市南沙区1\",\"remark\":\"11\",\"updateTime\":1608886392326,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"canteenSort\":1,\"createBy\":\"\",\"canteenTel\":\"10086\",\"createTime\":1608880693000,\"updateBy\":\"\",\"logo\":\"http://localhost:8080/profile/upload/2020/12/25/ff53ae3b-d890-4929-859e-15925e13093d.jpg\",\"id\":1,\"canteenLongitude\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:53:12');
INSERT INTO `sys_oper_log` VALUES (37, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_file_info', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:58:38');
INSERT INTO `sys_oper_log` VALUES (38, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件id\",\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":true,\"columnName\":\"file_id\"},{\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileName\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"文件名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_name\"},{\"usableColumn\":false,\"columnId\":45,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"filePath\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件路径\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_path\"},{\"usableColumn\":false,\"columnId\":46,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"create_time\"}],\"businessName\":\"info\",\"moduleName\":\"fb\",\"className\":\"SysFileInfo\",\"tableName\":\"sys_file_info\",\"crud\":true,\"options\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:59:15');
INSERT INTO `sys_oper_log` VALUES (39, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件id\",\"updateTime\":1608886755000,\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":true,\"columnName\":\"file_id\"},{\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileName\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"文件名称\",\"isQuery\":\"1\",\"updateTime\":1608886755000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_name\"},{\"usableColumn\":false,\"columnId\":45,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"filePath\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件路径\",\"updateTime\":1608886755000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_path\"},{\"usableColumn\":false,\"columnId\":46,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1608886755000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"create_time\"}],\"business', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:59:51');
INSERT INTO `sys_oper_log` VALUES (40, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件id\",\"updateTime\":1608886755000,\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":true,\"columnName\":\"file_id\"},{\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileName\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"文件名称\",\"isQuery\":\"1\",\"updateTime\":1608886755000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_name\"},{\"usableColumn\":false,\"columnId\":45,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"filePath\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件路径\",\"updateTime\":1608886755000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_path\"},{\"usableColumn\":false,\"columnId\":46,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1608886755000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"create_time\"}],\"business', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 16:59:51');
INSERT INTO `sys_oper_log` VALUES (41, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"1\",\"increment\":true,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件id\",\"updateTime\":1608886791000,\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":true,\"columnName\":\"file_id\"},{\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"fileName\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"文件名称\",\"isQuery\":\"1\",\"updateTime\":1608886791000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_name\"},{\"usableColumn\":false,\"columnId\":45,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"filePath\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"文件路径\",\"updateTime\":1608886791000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"file_path\"},{\"usableColumn\":false,\"columnId\":46,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1608886791000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1608886718000,\"tableId\":5,\"pk\":false,\"columnName\":\"create_time\"}],\"business', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:00:59');
INSERT INTO `sys_oper_log` VALUES (42, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-25 17:01:06');
INSERT INTO `sys_oper_log` VALUES (43, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"文件信息\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"fileinfo\",\"component\":\"system/fileinfo/index\",\"children\":[],\"createTime\":1608886908000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1080,\"menuType\":\"C\",\"perms\":\"system:fileinfo:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:18:53');
INSERT INTO `sys_oper_log` VALUES (44, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"upload\",\"orderNum\":\"1\",\"menuName\":\"文件信息\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"fileinfo\",\"component\":\"system/fileinfo/index\",\"children\":[],\"createTime\":1608886908000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1080,\"menuType\":\"C\",\"perms\":\"system:fileinfo:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-25 17:19:36');
INSERT INTO `sys_oper_log` VALUES (45, '字典类型', 1, 'com.wuhuacloud.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"餐类别\",\"params\":{},\"dictType\":\"fb_meal_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:27:24');
INSERT INTO `sys_oper_log` VALUES (46, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"Z\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"早餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:27:46');
INSERT INTO `sys_oper_log` VALUES (47, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"W\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"午餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:03');
INSERT INTO `sys_oper_log` VALUES (48, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"W\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"午餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964083000,\"dictCode\":33,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:09');
INSERT INTO `sys_oper_log` VALUES (49, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"X\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"下午茶\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:19');
INSERT INTO `sys_oper_log` VALUES (50, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"X\",\"dictSort\":2,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"下午茶\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964099000,\"dictCode\":34,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:29');
INSERT INTO `sys_oper_log` VALUES (51, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"W\",\"dictSort\":3,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"晚餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:40');
INSERT INTO `sys_oper_log` VALUES (52, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"ZAO\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"早餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964066000,\"dictCode\":32,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:28:55');
INSERT INTO `sys_oper_log` VALUES (53, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"WU\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"午餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964083000,\"dictCode\":33,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:29:00');
INSERT INTO `sys_oper_log` VALUES (54, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"XIA\",\"dictSort\":2,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"下午茶\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964099000,\"dictCode\":34,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:29:07');
INSERT INTO `sys_oper_log` VALUES (55, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"WAN\",\"dictSort\":3,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"晚餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964120000,\"dictCode\":35,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:29:14');
INSERT INTO `sys_oper_log` VALUES (56, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"ZHONG\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"中餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964083000,\"dictCode\":33,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:29:32');
INSERT INTO `sys_oper_log` VALUES (57, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zao\",\"dictSort\":0,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"早餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964066000,\"dictCode\":32,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:57:58');
INSERT INTO `sys_oper_log` VALUES (58, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zhone\",\"dictSort\":1,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"中餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964083000,\"dictCode\":33,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:58:04');
INSERT INTO `sys_oper_log` VALUES (59, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"xia\",\"dictSort\":2,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"下午茶\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964099000,\"dictCode\":34,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:58:13');
INSERT INTO `sys_oper_log` VALUES (60, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"wan\",\"dictSort\":3,\"params\":{},\"dictType\":\"fb_meal_type\",\"dictLabel\":\"晚餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1608964120000,\"dictCode\":35,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-26 14:58:20');
INSERT INTO `sys_oper_log` VALUES (61, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"\",\"mealDateFont\":\"2020-12-28\",\"menuType\":\"H\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2020-12-28 18:47:43');
INSERT INTO `sys_oper_log` VALUES (62, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-28\",\"menuType\":\"H\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2020-12-28 18:49:09');
INSERT INTO `sys_oper_log` VALUES (63, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-28\":{\"zhone\":[],\"xia\":[],\"wan\":[],\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":1}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2020-12-28 18:59:28');
INSERT INTO `sys_oper_log` VALUES (64, '代码生成', 3, 'com.wuhuacloud.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 22:44:05');
INSERT INTO `sys_oper_log` VALUES (65, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_canteen_menu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-28 22:44:10');
INSERT INTO `sys_oper_log` VALUES (66, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-28 22:44:18');
INSERT INTO `sys_oper_log` VALUES (67, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, 'java.lang.String cannot be cast to java.lang.Long', '2020-12-29 00:07:43');
INSERT INTO `sys_oper_log` VALUES (68, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, 'java.lang.String cannot be cast to java.lang.Long', '2020-12-29 00:07:57');
INSERT INTO `sys_oper_log` VALUES (69, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, 'java.lang.String cannot be cast to java.lang.Long', '2020-12-29 00:08:47');
INSERT INTO `sys_oper_log` VALUES (70, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, 'java.lang.String cannot be cast to java.lang.Long', '2020-12-29 00:10:30');
INSERT INTO `sys_oper_log` VALUES (71, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.CanteenMapper.editDayCanteenMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_canteen_menu          ( canteen_id,             type,             date )           values ( ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1\n; Data truncation: Data too long for column \'type\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1', '2020-12-29 00:28:09');
INSERT INTO `sys_oper_log` VALUES (72, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, 'can not cast to JSONObject.', '2020-12-29 00:29:05');
INSERT INTO `sys_oper_log` VALUES (73, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.CanteenMapper.editDayCanteenMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_canteen_menu          ( menu_id,             canteen_id,             type,             date,             menu_name,             menu_type,             order_num,             menu_price )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1\n; Data truncation: Data too long for column \'type\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'type\' at row 1', '2020-12-29 00:31:28');
INSERT INTO `sys_oper_log` VALUES (74, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: delete from fb_canteen_menu          WHERE  canteen_id=?                                           and type=?                                           and date=?\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'', '2020-12-29 00:42:23');
INSERT INTO `sys_oper_log` VALUES (75, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: delete from fb_canteen_menu          WHERE  canteen_id=?                                           and type=?                                           and date=?\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'type\' in \'where clause\'', '2020-12-29 00:44:31');
INSERT INTO `sys_oper_log` VALUES (76, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', 'null', 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'meal_type\' at row 1\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve com.wuhuacloud.fb.mapper.CanteenMapper.editDayCanteenMenu-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_canteen_menu          ( menu_id,             canteen_id,             meal_type,             meal_date,             menu_name,             menu_type,             order_num,             menu_price )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'meal_type\' at row 1\n; Data truncation: Data too long for column \'meal_type\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'meal_type\' at row 1', '2020-12-29 00:46:16');
INSERT INTO `sys_oper_log` VALUES (77, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\"}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 00:48:51');
INSERT INTO `sys_oper_log` VALUES (78, '代码生成', 3, 'com.wuhuacloud.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/6', '127.0.0.1', '内网IP', '{tableIds=6}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 00:53:55');
INSERT INTO `sys_oper_log` VALUES (79, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_canteen_menu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 00:54:01');
INSERT INTO `sys_oper_log` VALUES (80, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2020-12-29 00:54:05');
INSERT INTO `sys_oper_log` VALUES (81, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"H\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 11:11:45');
INSERT INTO `sys_oper_log` VALUES (82, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"H\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 15:51:03');
INSERT INTO `sys_oper_log` VALUES (83, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"H\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 15:51:42');
INSERT INTO `sys_oper_log` VALUES (84, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"H\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 15:52:07');
INSERT INTO `sys_oper_log` VALUES (85, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 18:34:47');
INSERT INTO `sys_oper_log` VALUES (86, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":18},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 18:48:08');
INSERT INTO `sys_oper_log` VALUES (87, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 18:55:04');
INSERT INTO `sys_oper_log` VALUES (88, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 18:58:41');
INSERT INTO `sys_oper_log` VALUES (89, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 19:05:59');
INSERT INTO `sys_oper_log` VALUES (90, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zao\":[{\"params\":{},\"menuId\":2,\"canteenId\":1,\"mealType\":\"zao\",\"mealDate\":\"2020-12-29\",\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"orderNum\":0,\"menuPrice\":15,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\"},{\"params\":{},\"menuId\":1,\"canteenId\":1,\"mealType\":\"zao\",\"mealDate\":\"2020-12-29\",\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"orderNum\":1,\"menuPrice\":15,\"canteenPrice\":19,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\"},{\"params\":{},\"menuId\":3,\"canteenId\":1,\"mealType\":\"zao\",\"mealDate\":\"2020-12-29\",\"menuName\":\"青菜\",\"menuType\":\"S\",\"orderNum\":0,\"menuPrice\":15,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\"},{\"params\":{},\"menuId\":4,\"canteenId\":1,\"mealType\":\"zao\",\"mealDate\":\"2020-12-29\",\"menuName\":\"测试1\",\"menuType\":\"Z\",\"orderNum\":0,\"menuPrice\":15,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\"}]}},\"canteenId\":1,\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:10:36');
INSERT INTO `sys_oper_log` VALUES (91, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"zhone\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zhone\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:12:38');
INSERT INTO `sys_oper_log` VALUES (92, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"wan\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"wan\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:12:49');
INSERT INTO `sys_oper_log` VALUES (93, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2020-12-29\":{\"xia\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"xia\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:12:53');
INSERT INTO `sys_oper_log` VALUES (94, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"wan\",\"menuId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:15:23');
INSERT INTO `sys_oper_log` VALUES (95, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"zao\",\"menuId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:26:02');
INSERT INTO `sys_oper_log` VALUES (96, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"wan\",\"menuId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:26:11');
INSERT INTO `sys_oper_log` VALUES (97, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"wan\",\"menuId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:26:12');
INSERT INTO `sys_oper_log` VALUES (98, '食堂菜单删除', 3, 'com.wuhuacloud.web.controller.fb.CanteenController.delCanteenMenut()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu/del', '127.0.0.1', '内网IP', '{\"canteenId\":\"1\",\"mealDateFont\":\"2020-12-29\",\"mealType\":\"wan\",\"menuId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2020-12-29 22:26:14');
INSERT INTO `sys_oper_log` VALUES (99, '字典类型', 1, 'com.wuhuacloud.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"交易类型\",\"params\":{},\"dictType\":\"trade_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:32:34');
INSERT INTO `sys_oper_log` VALUES (100, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"ZAO\",\"dictSort\":0,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"早餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:33:49');
INSERT INTO `sys_oper_log` VALUES (101, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"WU\",\"dictSort\":1,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"午餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:34:00');
INSERT INTO `sys_oper_log` VALUES (102, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"WAN\",\"dictSort\":2,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"晚餐\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:34:15');
INSERT INTO `sys_oper_log` VALUES (103, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"XIAO\",\"dictSort\":4,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"小卖部\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:34:29');
INSERT INTO `sys_oper_log` VALUES (104, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zao\",\"dictSort\":0,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"早餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1609731229000,\"dictCode\":36,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:35:33');
INSERT INTO `sys_oper_log` VALUES (105, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"wu\",\"dictSort\":1,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"午餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1609731240000,\"dictCode\":37,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:35:38');
INSERT INTO `sys_oper_log` VALUES (106, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"wan\",\"dictSort\":2,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"晚餐\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1609731255000,\"dictCode\":38,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:35:48');
INSERT INTO `sys_oper_log` VALUES (107, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"xiao\",\"dictSort\":4,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"小卖部\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1609731269000,\"dictCode\":39,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:35:58');
INSERT INTO `sys_oper_log` VALUES (108, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"chong\",\"dictSort\":4,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"充值\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:37:46');
INSERT INTO `sys_oper_log` VALUES (109, '字典数据', 1, 'com.wuhuacloud.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"jing\",\"dictSort\":6,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"津贴\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:38:16');
INSERT INTO `sys_oper_log` VALUES (110, '字典数据', 2, 'com.wuhuacloud.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"jing\",\"dictSort\":5,\"params\":{},\"dictType\":\"trade_type\",\"dictLabel\":\"津贴\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1609731496000,\"dictCode\":41,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 11:38:21');
INSERT INTO `sys_oper_log` VALUES (111, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_trade_info,fb_wallet', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:42:32');
INSERT INTO `sys_oper_log` VALUES (112, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":66,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"tradeDime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"交易时间\",\"isQuery\":\"1\",\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"trade_dime\"},{\"usableColumn\":false,\"columnId\":67,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"tradeIp\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"交易IP\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"trade_ip\"},{\"usableColumn\":false,\"columnId\":68,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":69,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":70,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:44:45');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":77,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"user_id\"},{\"usableColumn\":false,\"columnId\":78,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":79,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":80,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"updateBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"更新者\",\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"update_by\"},{\"usableColumn\":false,\"columnId\":81,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"up', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:45:24');
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-01-04 13:45:34');
INSERT INTO `sys_oper_log` VALUES (115, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-01-04 13:45:36');
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":77,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"updateTime\":1609739124000,\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"user_id\"},{\"usableColumn\":false,\"columnId\":78,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"updateTime\":1609739124000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":79,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1609739124000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"columnId\":80,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"updateBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"更新者\",\"updateTime\":1609739124000,\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":9,\"pk\":false,\"columnName\":\"update_by\"},{\"usableColumn\":false,\"columnId\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:46:06');
INSERT INTO `sys_oper_log` VALUES (117, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":66,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"tradeDime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"交易时间\",\"isQuery\":\"1\",\"updateTime\":1609739085000,\"sort\":1,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"trade_dime\"},{\"usableColumn\":false,\"columnId\":67,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"tradeIp\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"交易IP\",\"updateTime\":1609739085000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"trade_ip\"},{\"usableColumn\":false,\"columnId\":68,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"updateTime\":1609739085000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":69,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1609739085000,\"sort\":4,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609738952000,\"tableId\":8,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":false,\"column', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:46:27');
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"交易记录\",\"params\":{},\"parentId\":1061,\"isCache\":\"0\",\"path\":\"info\",\"component\":\"fb/info/index\",\"children\":[],\"createTime\":1609739219000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1086,\"menuType\":\"C\",\"perms\":\"fb:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:58:52');
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"余额\",\"params\":{},\"parentId\":1061,\"isCache\":\"0\",\"path\":\"wallet\",\"component\":\"fb/wallet/index\",\"children\":[],\"createTime\":1609739268000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1092,\"menuType\":\"C\",\"perms\":\"fb:wallet:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-04 13:59:00');
INSERT INTO `sys_oper_log` VALUES (120, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'canteen_time', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 09:59:10');
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":84,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898349000,\"tableId\":10,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":85,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898350000,\"tableId\":10,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":true,\"columnId\":86,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"remark\",\"htmlType\":\"textarea\",\"edit\":false,\"query\":false,\"columnComment\":\"备注\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(500)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898350000,\"tableId\":10,\"pk\":false,\"columnName\":\"remark\"},{\"usableColumn\":false,\"columnId\":87,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"canteenId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"食堂编号\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898350000,\"tableId\":10,\"pk\":false,\"columnName\":\"canteen_id\"},{\"usableColumn\":false,\"columnId\":88,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"requi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 09:59:52');
INSERT INTO `sys_oper_log` VALUES (122, '代码生成', 3, 'com.wuhuacloud.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/10', '127.0.0.1', '内网IP', '{tableIds=10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 10:00:37');
INSERT INTO `sys_oper_log` VALUES (123, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_canteen_time', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 10:00:54');
INSERT INTO `sys_oper_log` VALUES (124, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":91,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":92,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":true,\"columnId\":93,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"remark\",\"htmlType\":\"textarea\",\"edit\":false,\"query\":false,\"columnComment\":\"备注\",\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(500)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"remark\"},{\"usableColumn\":false,\"columnId\":94,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"canteenId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"食堂编号\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"canteen_id\"},{\"usableColumn\":false,\"columnId\":95,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"requi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 10:01:26');
INSERT INTO `sys_oper_log` VALUES (125, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"functionAuthor\":\"wenfl\",\"columns\":[{\"usableColumn\":false,\"columnId\":91,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建者\",\"updateTime\":1609898486000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(64)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"create_by\"},{\"usableColumn\":false,\"columnId\":92,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1609898486000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"create_time\"},{\"usableColumn\":true,\"columnId\":93,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"remark\",\"htmlType\":\"textarea\",\"edit\":false,\"query\":false,\"columnComment\":\"备注\",\"updateTime\":1609898486000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(500)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"remark\"},{\"usableColumn\":false,\"columnId\":94,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"canteenId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"食堂编号\",\"isQuery\":\"1\",\"updateTime\":1609898486000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1609898454000,\"tableId\":11,\"pk\":false,\"columnName\":\"canteen_id\"},{\"usableCol', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 10:02:04');
INSERT INTO `sys_oper_log` VALUES (126, '代码生成', 8, 'com.wuhuacloud.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2021-01-06 10:02:21');
INSERT INTO `sys_oper_log` VALUES (127, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createTime\":1609904871408,\"mealType\":\"wan\",\"startTime\":\"08:45\",\"stopTime\":\"09:15\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609904873277,\"mealType\":\"wan\",\"startTime\":\"09:00\",\"stopTime\":\"09:15\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609904874339,\"mealType\":\"wan\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609904875452,\"mealType\":\"wan\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 11:48:00');
INSERT INTO `sys_oper_log` VALUES (128, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createTime\":1609911684168,\"mealType\":\"zao\",\"startTime\":\"06:00\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609911684178,\"mealType\":\"zhone\",\"startTime\":\"11:45\",\"stopTime\":\"12:00\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609911684183,\"mealType\":\"xia\",\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createTime\":1609911684188,\"mealType\":\"wan\",\"startTime\":\"18:00\",\"stopTime\":\"18:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 13:41:24');
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 3, 'com.wuhuacloud.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/11', '127.0.0.1', '内网IP', '{tableIds=11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 13:47:28');
INSERT INTO `sys_oper_log` VALUES (130, '代码生成', 6, 'com.wuhuacloud.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'fb_canteen_time', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 13:47:32');
INSERT INTO `sys_oper_log` VALUES (131, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913453586,\"mealType\":\"zao\",\"orderNum\":1,\"startTime\":\"06:00\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913453597,\"mealType\":\"zhone\",\"orderNum\":2,\"startTime\":\"11:45\",\"stopTime\":\"12:00\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913453602,\"mealType\":\"xia\",\"orderNum\":3,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913453606,\"mealType\":\"wan\",\"orderNum\":4,\"startTime\":\"18:00\",\"stopTime\":\"18:15\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:10:53');
INSERT INTO `sys_oper_log` VALUES (132, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714681,\"mealType\":\"zao\",\"orderNum\":1,\"startTime\":\"06:00\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714695,\"mealType\":\"zao\",\"orderNum\":1,\"startTime\":\"06:00\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714704,\"mealType\":\"zhone\",\"orderNum\":2,\"startTime\":\"11:45\",\"stopTime\":\"12:00\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714712,\"mealType\":\"zhone\",\"orderNum\":2,\"startTime\":\"11:45\",\"stopTime\":\"12:00\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714719,\"mealType\":\"xia\",\"orderNum\":3,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714727,\"mealType\":\"xia\",\"orderNum\":3,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714733,\"mealType\":\"wan\",\"orderNum\":4,\"startTime\":\"18:00\",\"stopTime\":\"18:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913714739,\"mealType\":\"wan\",\"orderNum\":4,\"startTime\":\"18:00\",\"stopTime\":\"18:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:15:14');
INSERT INTO `sys_oper_log` VALUES (133, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913861324,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"07:00\",\"stopTime\":\"07:45\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913861329,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:15\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913861333,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913861339,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:17:41');
INSERT INTO `sys_oper_log` VALUES (134, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913879904,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"07:00\",\"stopTime\":\"07:45\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913879908,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:15\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913879914,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1609913879918,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:15\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:17:59');
INSERT INTO `sys_oper_log` VALUES (135, '食堂营业时间', 1, 'com.wuhuacloud.fb.controller.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":2,\"createBy\":\"admin\",\"createTime\":1609913924302,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"07:00\",\"stopTime\":\"07:30\",\"params\":{}},{\"canteenId\":2,\"createBy\":\"admin\",\"createTime\":1609913924306,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:15\",\"params\":{}},{\"canteenId\":2,\"createBy\":\"admin\",\"createTime\":1609913924310,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"15:30\",\"stopTime\":\"16:00\",\"params\":{}},{\"canteenId\":2,\"createBy\":\"admin\",\"createTime\":1609913924314,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:30\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:18:44');
INSERT INTO `sys_oper_log` VALUES (136, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1098', '127.0.0.1', '内网IP', '{menuId=1098}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', 0, NULL, '2021-01-06 14:29:05');
INSERT INTO `sys_oper_log` VALUES (137, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1099', '127.0.0.1', '内网IP', '{menuId=1099}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:16');
INSERT INTO `sys_oper_log` VALUES (138, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1103', '127.0.0.1', '内网IP', '{menuId=1103}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:19');
INSERT INTO `sys_oper_log` VALUES (139, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1102', '127.0.0.1', '内网IP', '{menuId=1102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:21');
INSERT INTO `sys_oper_log` VALUES (140, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1101', '127.0.0.1', '内网IP', '{menuId=1101}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:23');
INSERT INTO `sys_oper_log` VALUES (141, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1100', '127.0.0.1', '内网IP', '{menuId=1100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:25');
INSERT INTO `sys_oper_log` VALUES (142, '菜单管理', 3, 'com.wuhuacloud.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1098', '127.0.0.1', '内网IP', '{menuId=1098}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-06 14:29:27');
INSERT INTO `sys_oper_log` VALUES (143, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[1,2]}', 'null', 1, 'java.lang.Integer cannot be cast to java.lang.Long', '2021-01-07 14:36:43');
INSERT INTO `sys_oper_log` VALUES (144, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[1,2]}', 'null', 1, 'java.lang.Integer cannot be cast to java.lang.Long', '2021-01-07 14:37:17');
INSERT INTO `sys_oper_log` VALUES (145, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[1,2]}', 'null', 1, 'java.util.ArrayList cannot be cast to [Ljava.lang.String;', '2021-01-07 14:45:28');
INSERT INTO `sys_oper_log` VALUES (146, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[1,2]}', 'null', 1, 'java.util.ArrayList cannot be cast to [Ljava.lang.String;', '2021-01-07 14:46:35');
INSERT INTO `sys_oper_log` VALUES (147, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":101,\"canteens\":[\"\",\"\"]}', 'null', 1, 'java.util.ArrayList cannot be cast to [Ljava.lang.String;', '2021-01-07 14:48:17');
INSERT INTO `sys_oper_log` VALUES (148, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[\"1\",\"2\"]}', 'null', 1, 'java.util.ArrayList cannot be cast to [Ljava.lang.String;', '2021-01-07 14:48:55');
INSERT INTO `sys_oper_log` VALUES (149, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[\"1\",\"2\"]}', 'null', 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'deptId\' not found. Available parameters are [arg1, arg0, param1, param2]', '2021-01-07 15:03:41');
INSERT INTO `sys_oper_log` VALUES (150, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[\"1\",\"2\"]}', 'null', 1, '\r\n### Error updating database.  Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'100\' for key \'PRIMARY\'\r\n### The error may exist in file [E:\\wfl\\IdeaProjects\\fb\\fb-business\\target\\classes\\mapper\\fb\\CanteenMapper.xml]\r\n### The error may involve defaultParameterMap\r\n### The error occurred while setting parameters\r\n### SQL: insert into fb_canteen_dept (dept_id,canteen_id) values (?,?)\r\n### Cause: java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'100\' for key \'PRIMARY\'\n; Duplicate entry \'100\' for key \'PRIMARY\'; nested exception is java.sql.SQLIntegrityConstraintViolationException: Duplicate entry \'100\' for key \'PRIMARY\'', '2021-01-07 15:12:24');
INSERT INTO `sys_oper_log` VALUES (151, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[\"1\",\"2\"]}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-07 15:12:57');
INSERT INTO `sys_oper_log` VALUES (152, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":100,\"canteens\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-07 15:13:14');
INSERT INTO `sys_oper_log` VALUES (153, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":103,\"canteens\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-07 15:16:15');
INSERT INTO `sys_oper_log` VALUES (154, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fb_suggest', '127.0.0.1', '内网IP', '{tableName=fb_suggest}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 16:43:09');
INSERT INTO `sys_oper_log` VALUES (155, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"超级管理员\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员角色', '2021-01-08 17:25:41');
INSERT INTO `sys_oper_log` VALUES (156, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$ITVVu.w3B4QeoJw6jGepBemXoC/QUbRIbudTMs8D.KCiCQXW579SC\",\"postIds\":[],\"nickName\":\"温发良\",\"deptId\":100,\"params\":{},\"userName\":\"wenfl\",\"userId\":3,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 17:49:46');
INSERT INTO `sys_oper_log` VALUES (157, '用户管理', 2, 'com.wuhuacloud.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[],\"phonenumber\":\"\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[],\"loginIp\":\"\",\"email\":\"\",\"nickName\":\"温发良\",\"sex\":\"0\",\"deptId\":100,\"avatar\":\"\",\"dept\":{\"deptName\":\"若依科技\",\"leader\":\"若依\",\"deptId\":100,\"orderNum\":\"0\",\"params\":{},\"parentId\":0,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"wenfl\",\"userId\":3,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1610099386000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:01:58');
INSERT INTO `sys_oper_log` VALUES (158, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:02:57');
INSERT INTO `sys_oper_log` VALUES (159, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:03:42');
INSERT INTO `sys_oper_log` VALUES (160, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:04:10');
INSERT INTO `sys_oper_log` VALUES (161, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:06:05');
INSERT INTO `sys_oper_log` VALUES (162, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:21:38');
INSERT INTO `sys_oper_log` VALUES (163, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:32:42');
INSERT INTO `sys_oper_log` VALUES (164, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104,105,106,107,102,108,109],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:33:25');
INSERT INTO `sys_oper_log` VALUES (165, '通知公告', 1, 'com.wuhuacloud.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"params\":{},\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"deptIds\":[100,101,103,104,105,106,107],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 18:36:09');
INSERT INTO `sys_oper_log` VALUES (166, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"0\",\"noticeType\":\"1\",\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:03:44');
INSERT INTO `sys_oper_log` VALUES (167, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,101,103,104,105\",\"noticeType\":\"1\",\"updateTime\":1610103824000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:23:56');
INSERT INTO `sys_oper_log` VALUES (168, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,102,108,109\",\"noticeType\":\"1\",\"updateTime\":1610105036000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:34:48');
INSERT INTO `sys_oper_log` VALUES (169, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,101,104\",\"noticeType\":\"1\",\"updateTime\":1610105688000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:35:21');
INSERT INTO `sys_oper_log` VALUES (170, '角色管理', 2, 'com.wuhuacloud.web.controller.system.SysRoleController.dataScope()', 'PUT', 1, 'admin', NULL, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1608544287000,\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"deptIds\":[100,101,103,104],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:35:51');
INSERT INTO `sys_oper_log` VALUES (171, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,102,108,109\",\"noticeType\":\"1\",\"updateTime\":1610105721000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 19:37:12');
INSERT INTO `sys_oper_log` VALUES (172, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,102,108\",\"noticeType\":\"1\",\"updateTime\":1610105832000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 22:43:27');
INSERT INTO `sys_oper_log` VALUES (173, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"103,108\",\"noticeType\":\"1\",\"updateTime\":1610117007000,\"params\":{},\"noticeId\":3,\"noticeTitle\":\"我是测试\",\"noticeContent\":\"<p>测试</p>\",\"createBy\":\"admin\",\"createTime\":1610102169000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 23:06:27');
INSERT INTO `sys_oper_log` VALUES (174, '通知公告', 1, 'com.wuhuacloud.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"104,105\",\"noticeType\":\"1\",\"params\":{},\"noticeTitle\":\"更新通知\",\"noticeContent\":\"<p>各位请注意，我马上要更新了</p>\",\"createBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-08 23:08:48');
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 2, 'com.wuhuacloud.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/fb_trade_info', '127.0.0.1', '内网IP', '{tableName=fb_trade_info}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-09 21:59:21');
INSERT INTO `sys_oper_log` VALUES (176, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/109', '127.0.0.1', '内网IP', '{deptId=109}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:39');
INSERT INTO `sys_oper_log` VALUES (177, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/108', '127.0.0.1', '内网IP', '{deptId=108}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:41');
INSERT INTO `sys_oper_log` VALUES (178, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/102', '127.0.0.1', '内网IP', '{deptId=102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:43');
INSERT INTO `sys_oper_log` VALUES (179, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/107', '127.0.0.1', '内网IP', '{deptId=107}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:44');
INSERT INTO `sys_oper_log` VALUES (180, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/106', '127.0.0.1', '内网IP', '{deptId=106}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:45');
INSERT INTO `sys_oper_log` VALUES (181, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/105', '127.0.0.1', '内网IP', '{deptId=105}', '{\"msg\":\"部门存在用户,不允许删除\",\"code\":500}', 0, NULL, '2021-01-12 10:05:47');
INSERT INTO `sys_oper_log` VALUES (182, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/105', '127.0.0.1', '内网IP', '{deptId=105}', '{\"msg\":\"部门存在用户,不允许删除\",\"code\":500}', 0, NULL, '2021-01-12 10:05:49');
INSERT INTO `sys_oper_log` VALUES (183, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/104', '127.0.0.1', '内网IP', '{deptId=104}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:05:50');
INSERT INTO `sys_oper_log` VALUES (184, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/105', '127.0.0.1', '内网IP', '{deptId=105}', '{\"msg\":\"部门存在用户,不允许删除\",\"code\":500}', 0, NULL, '2021-01-12 10:05:53');
INSERT INTO `sys_oper_log` VALUES (185, '部门管理', 2, 'com.wuhuacloud.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"广五集团\",\"leader\":\"温生\",\"deptId\":100,\"orderNum\":\"0\",\"delFlag\":\"0\",\"params\":{},\"parentId\":0,\"createBy\":\"admin\",\"children\":[],\"createTime\":1608544287000,\"phone\":\"13800138000\",\"updateBy\":\"admin\",\"ancestors\":\"0\",\"email\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:06:33');
INSERT INTO `sys_oper_log` VALUES (186, '部门管理', 2, 'com.wuhuacloud.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"技术部\",\"leader\":\"\",\"deptId\":101,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1608544287000,\"phone\":\"\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:07:18');
INSERT INTO `sys_oper_log` VALUES (187, '部门管理', 2, 'com.wuhuacloud.web.controller.system.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"广东五华云计算有限公司\",\"leader\":\"\",\"deptId\":101,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1608544287000,\"phone\":\"\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:09:10');
INSERT INTO `sys_oper_log` VALUES (188, '用户管理', 3, 'com.wuhuacloud.web.controller.system.SysUserController.remove()', 'DELETE', 1, 'admin', NULL, '/system/user/2', '127.0.0.1', '内网IP', '{userIds=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:09:29');
INSERT INTO `sys_oper_log` VALUES (189, '用户管理', 2, 'com.wuhuacloud.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[],\"loginIp\":\"\",\"email\":\"\",\"nickName\":\"温发良\",\"sex\":\"0\",\"deptId\":101,\"avatar\":\"\",\"dept\":{\"deptName\":\"广五集团\",\"leader\":\"温生\",\"deptId\":100,\"orderNum\":\"0\",\"params\":{},\"parentId\":0,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"wenfl\",\"userId\":3,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1610099386000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:09:50');
INSERT INTO `sys_oper_log` VALUES (190, '部门管理', 3, 'com.wuhuacloud.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/105', '127.0.0.1', '内网IP', '{deptId=105}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:10:01');
INSERT INTO `sys_oper_log` VALUES (191, '用户管理', 2, 'com.wuhuacloud.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[],\"loginIp\":\"\",\"email\":\"\",\"nickName\":\"温发良\",\"sex\":\"0\",\"deptId\":103,\"avatar\":\"\",\"dept\":{\"deptName\":\"广东五华云计算有限公司\",\"leader\":\"\",\"deptId\":101,\"orderNum\":\"1\",\"params\":{},\"parentId\":100,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"wenfl\",\"userId\":3,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1610099386000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:12:30');
INSERT INTO `sys_oper_log` VALUES (192, '用户管理', 2, 'com.wuhuacloud.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"phonenumber\":\"\",\"admin\":true,\"loginDate\":1608544287000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"password\":\"\",\"postIds\":[],\"loginIp\":\"127.0.0.1\",\"email\":\"\",\"nickName\":\"超级管理员\",\"sex\":\"0\",\"deptId\":103,\"avatar\":\"/profile/avatar/2020/12/25/d03fe7bd-6be4-4706-b2b9-918b17ca2f7c.jpeg\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"roleIds\":[1],\"createTime\":1608544287000,\"status\":\"0\"}', 'null', 1, '不允许操作超级管理员用户', '2021-01-12 10:13:06');
INSERT INTO `sys_oper_log` VALUES (193, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$hKr2poFT37mimLu.goQFtuNqAGhXzv2SJxHPC8W0UBprtAgKAYsF.\",\"postIds\":[],\"nickName\":\"蒋雷\",\"sex\":\"2\",\"deptId\":103,\"params\":{},\"userName\":\"jiangl\",\"userId\":4,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:15:07');
INSERT INTO `sys_oper_log` VALUES (194, '角色管理', 1, 'com.wuhuacloud.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":3,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"canteen\",\"roleName\":\"食堂管理员\",\"deptIds\":[],\"menuIds\":[1061,1062,1086,1063,1064,1065,1067,1068,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1087,1091,1092,1093,1094,1095,1096,1097],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:16:57');
INSERT INTO `sys_oper_log` VALUES (195, '用户管理', 2, 'com.wuhuacloud.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[],\"loginIp\":\"\",\"email\":\"\",\"nickName\":\"蒋雷\",\"sex\":\"2\",\"deptId\":103,\"avatar\":\"\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"jiangl\",\"userId\":4,\"createBy\":\"admin\",\"roleIds\":[2,3],\"createTime\":1610417707000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:23:49');
INSERT INTO `sys_oper_log` VALUES (196, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$jzPSJaf9HBuRvU6ZVHBJw.2M8IpCcL52BX9iTMOp3j2VeVr/Nnd8W\",\"postIds\":[],\"nickName\":\"吴博涛\",\"sex\":\"2\",\"deptId\":103,\"params\":{},\"userName\":\"wubt\",\"userId\":5,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:24:16');
INSERT INTO `sys_oper_log` VALUES (197, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$vX6p2h7f.To.AcyQrlqk4.FH.n3/a446elS/8wUV6w0wbLTdwBYHy\",\"postIds\":[],\"nickName\":\"赵亚强\",\"deptId\":103,\"params\":{},\"userName\":\"zhaoyq\",\"userId\":6,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:24:48');
INSERT INTO `sys_oper_log` VALUES (198, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$dL7VcPEW4rD1.xrhgkc0zeAocby4M6x7Yq85I0GGjrlps1NBxZXoS\",\"postIds\":[],\"nickName\":\"陈仪椿\",\"deptId\":103,\"params\":{},\"userName\":\"chenyc\",\"userId\":7,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 10:51:15');
INSERT INTO `sys_oper_log` VALUES (199, '食堂', 3, 'com.wuhuacloud.fb.controller.CanteenController.remove()', 'DELETE', 1, 'admin', NULL, '/fb/canteen/1,2', '127.0.0.1', '内网IP', '{ids=1,2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 11:01:52');
INSERT INTO `sys_oper_log` VALUES (200, '食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"集团食堂\",\"canteenAddr\":\"中国南沙\",\"params\":{},\"canteenSort\":0,\"canteenTel\":\"13800138000\",\"createTime\":1610420538157,\"id\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 11:02:18');
INSERT INTO `sys_oper_log` VALUES (201, '食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"云计算公司\",\"canteenPrincipal\":\"温生\",\"canteenAddr\":\"广州南沙\",\"params\":{},\"canteenSort\":1,\"canteenTel\":\"13800138000\",\"createTime\":1610420636679,\"id\":4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 11:03:57');
INSERT INTO `sys_oper_log` VALUES (202, '食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen', '127.0.0.1', '内网IP', '{\"canteenStatus\":\"0\",\"canteenName\":\"知产公司\",\"canteenAddr\":\"广州\",\"params\":{},\"canteenSort\":3,\"canteenTel\":\"13800138000\",\"createTime\":1610420669841,\"id\":5}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 11:04:30');
INSERT INTO `sys_oper_log` VALUES (203, '余额', 5, 'com.wuhuacloud.fb.controller.WalletController.export()', 'GET', 1, 'admin', NULL, '/fb/wallet/export', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"15f824a2-7511-4861-99af-7ea15d39260c_wallet.xlsx\",\"code\":200}', 0, NULL, '2021-01-12 14:04:39');
INSERT INTO `sys_oper_log` VALUES (204, '部门绑定食堂', 1, 'com.wuhuacloud.fb.controller.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":103,\"canteens\":[\"4\",\"5\",\"3\"]}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 17:34:29');
INSERT INTO `sys_oper_log` VALUES (205, '用户管理', 1, 'com.wuhuacloud.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$c5tihBbJE5I8zScy8P1rmeUItHPBqNj/cq5uNVd8KyMk4AUapMou.\",\"postIds\":[],\"nickName\":\"丁卫\",\"params\":{},\"userName\":\"dingw\",\"userId\":900000,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-12 18:39:35');
INSERT INTO `sys_oper_log` VALUES (206, '食堂营业时间', 1, 'com.wuhuacloud.web.controller.fb.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610547934241,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"06:30\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610547934259,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610547934268,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610547934277,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-13 22:25:34');
INSERT INTO `sys_oper_log` VALUES (207, '食堂营业时间', 1, 'com.wuhuacloud.web.controller.fb.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":5,\"createBy\":\"admin\",\"createTime\":1610548003661,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"06:30\",\"stopTime\":\"08:00\",\"params\":{}},{\"canteenId\":5,\"createBy\":\"admin\",\"createTime\":1610548003671,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:30\",\"stopTime\":\"13:30\",\"params\":{}},{\"canteenId\":5,\"createBy\":\"admin\",\"createTime\":1610548003680,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:45\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":5,\"createBy\":\"admin\",\"createTime\":1610548003689,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-13 22:26:43');
INSERT INTO `sys_oper_log` VALUES (208, '食堂营业时间', 1, 'com.wuhuacloud.web.controller.fb.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":3,\"createBy\":\"admin\",\"createTime\":1610548078651,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"06:00\",\"stopTime\":\"08:00\",\"params\":{}},{\"canteenId\":3,\"createBy\":\"admin\",\"createTime\":1610548078664,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:30\",\"params\":{}},{\"canteenId\":3,\"createBy\":\"admin\",\"createTime\":1610548078674,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"15:00\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":3,\"createBy\":\"admin\",\"createTime\":1610548078683,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:00\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-13 22:27:58');
INSERT INTO `sys_oper_log` VALUES (209, '食堂营业时间', 1, 'com.wuhuacloud.web.controller.fb.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610691083421,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"06:30\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610691083430,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610691083439,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:00\",\"stopTime\":\"15:30\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610691083448,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-15 14:11:23');
INSERT INTO `sys_oper_log` VALUES (210, '食堂营业时间', 1, 'com.wuhuacloud.web.controller.fb.CanteenTimeController.add()', 'POST', 1, 'admin', NULL, '/fb/canteen_time', '127.0.0.1', '内网IP', '[{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610696337969,\"mealType\":\"zao\",\"orderNum\":0,\"startTime\":\"06:30\",\"stopTime\":\"08:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610696337978,\"mealType\":\"zhone\",\"orderNum\":1,\"startTime\":\"11:45\",\"stopTime\":\"12:45\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610696337987,\"mealType\":\"xia\",\"orderNum\":2,\"startTime\":\"14:00\",\"stopTime\":\"16:00\",\"params\":{}},{\"canteenId\":4,\"createBy\":\"admin\",\"createTime\":1610696337996,\"mealType\":\"wan\",\"orderNum\":3,\"startTime\":\"18:00\",\"stopTime\":\"19:30\",\"params\":{}}]', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-15 15:38:58');
INSERT INTO `sys_oper_log` VALUES (211, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"\",\"noticeType\":\"2\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":1,\"noticeTitle\":\"版本更新\",\"noticeContent\":\"<p>温馨提醒：2018-07-01 若依新版本发布啦</p>\",\"createBy\":\"admin\",\"createTime\":1608544288000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-17 00:45:15');
INSERT INTO `sys_oper_log` VALUES (212, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,101,103\",\"noticeType\":\"1\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":2,\"noticeTitle\":\"维护通知\",\"noticeContent\":\"<p>维护通知：2018-07-01 若依系统凌晨维护</p>\",\"createBy\":\"admin\",\"createTime\":1608544288000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-17 00:45:31');
INSERT INTO `sys_oper_log` VALUES (213, '通知公告', 2, 'com.wuhuacloud.web.controller.system.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"deptId\":\"100,101,103\",\"noticeType\":\"1\",\"params\":{},\"noticeId\":4,\"noticeTitle\":\"更新通知\",\"noticeContent\":\"<p>各位请注意，我马上要更新了</p>\",\"createBy\":\"admin\",\"createTime\":1610118528000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-17 00:45:43');
INSERT INTO `sys_oper_log` VALUES (214, '意见', 2, 'com.wuhuacloud.web.controller.fb.SuggestController.edit()', 'PUT', 1, 'admin', NULL, '/fb/suggest', '127.0.0.1', '内网IP', '{\"canteenId\":1,\"createBy\":\"admin\",\"createTime\":1610097483000,\"updateBy\":\"\",\"remark\":\"我是测试\",\"updateTime\":1610938062022,\"replay\":\"没有意见\",\"params\":{},\"suggestId\":\"8730cec0c18c4d2fb02ccbaa4a162a1e\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-18 10:47:41');
INSERT INTO `sys_oper_log` VALUES (215, '菜单管理', 1, 'com.wuhuacloud.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"upload\",\"orderNum\":\"1\",\"menuName\":\"文件上传\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"oss/oss\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-19 11:32:19');
INSERT INTO `sys_oper_log` VALUES (216, '菜单管理', 2, 'com.wuhuacloud.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"upload\",\"orderNum\":\"1\",\"menuName\":\"文件上传\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"oss\",\"component\":\"system/oss/oss\",\"children\":[],\"createTime\":1611027139000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1098,\"menuType\":\"C\",\"perms\":\"system:oss:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-19 11:34:30');
INSERT INTO `sys_oper_log` VALUES (217, '部门绑定食堂', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.addCanteenDept()', 'POST', 1, 'admin', NULL, '/fb/canteen/saveCanteenDept', '127.0.0.1', '内网IP', '{\"deptId\":103,\"canteens\":[\"5\",\"3\"]}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-20 17:12:15');
INSERT INTO `sys_oper_log` VALUES (218, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-21\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15}]}},\"canteenId\":4,\"mealDateFont\":\"2021-01-21\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-21 17:30:24');
INSERT INTO `sys_oper_log` VALUES (219, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-21\":{\"zhone\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":4,\"mealDateFont\":\"2021-01-21\",\"mealType\":\"zhone\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-21 17:30:59');
INSERT INTO `sys_oper_log` VALUES (220, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-20\":{\"zhone\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":4,\"mealDateFont\":\"2021-01-20\",\"mealType\":\"zhone\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-21 17:31:09');
INSERT INTO `sys_oper_log` VALUES (221, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-20\":{\"xia\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":4,\"mealDateFont\":\"2021-01-20\",\"mealType\":\"xia\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-21 17:31:22');
INSERT INTO `sys_oper_log` VALUES (222, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-21\":{\"xia\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15}]}},\"canteenId\":4,\"mealDateFont\":\"2021-01-21\",\"mealType\":\"xia\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-21 17:48:49');
INSERT INTO `sys_oper_log` VALUES (223, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":3,\"mealDateFont\":\"2021-01-23\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 11:00:44');
INSERT INTO `sys_oper_log` VALUES (224, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"me', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 11:00:51');
INSERT INTO `sys_oper_log` VALUES (225, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"zhone\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15}]}},\"canteenId\":3,\"mealDateFont\":\"2021-01-23\",\"mealType\":\"zhone\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 11:00:59');
INSERT INTO `sys_oper_log` VALUES (226, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"xia\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:43\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":3,\"menuName\":\"青菜\",\"menuType\":\"S\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:12:00\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":4,\"menuName\":\"测试1\",\"menuType\":\"Z\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":3,\"mealDateFont\":\"2021-01-23\",\"mealType\":\"xia\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 11:01:05');
INSERT INTO `sys_oper_log` VALUES (227, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"wan\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15},{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:11:26\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":2,\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":3,\"mealDateFont\":\"2021-01-23\",\"mealType\":\"wan\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 11:01:12');
INSERT INTO `sys_oper_log` VALUES (228, '食堂菜单', 1, 'com.wuhuacloud.web.controller.fb.MenuController.add()', 'POST', 1, 'admin', NULL, '/fb/menu', '127.0.0.1', '内网IP', '{\"menuImg\":\"http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif\",\"menuPrice\":20,\"orderNum\":0,\"menuName\":\"大刀肉\",\"params\":{},\"createTime\":1611381145143,\"menuId\":5,\"menuType\":\"H\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 13:52:26');
INSERT INTO `sys_oper_log` VALUES (229, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '127.0.0.1', '内网IP', '{\"data\":{\"2021-01-23\":{\"zao\":[{\"params\":{},\"menuId\":4,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"测试1\",\"menuType\":\"Z\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/684e8690-4ad3-4d03-94be-a57545ea78e8.jpg\"},{\"params\":{},\"menuId\":3,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"青菜\",\"menuType\":\"S\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\"},{\"params\":{},\"menuId\":2,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\"},{\"params\":{},\"menuId\":1,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"orderNum\":1,\"menuPrice\":15,\"canteenPrice\":15,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\"},{\"params\":{},\"menuId\":1,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"orderNum\":1,\"menuPrice\":15,\"canteenPrice\":15,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\"},{\"params\":{},\"menuId\":2,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"红烧排骨\",\"menuType\":\"H\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/309f71fc-ffb3-48b4-a93a-69090c88cc32.jpg\"},{\"params\":{},\"menuId\":3,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"青菜\",\"menuType\":\"S\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/e7637550-a2d8-4591-b7e6-0bd617e50394.jpg\"},{\"params\":{},\"menuId\":4,\"canteenId\":3,\"mealType\":\"zao\",\"mealDate\":\"2021-01-23\",\"menuName\":\"测试1\",\"menuType\":\"Z\",\"orderNum\":0,\"menuPrice\":20,\"canteenPrice\":20,', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-01-23 13:52:55');
INSERT INTO `sys_oper_log` VALUES (230, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '116.23.97.237', 'XX XX', '{\"data\":{\"2021-02-01\":{\"zao\":[{\"createBy\":\"\",\"createTime\":\"2020-12-25 16:02:17\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":1,\"menuName\":\"红烧肉\",\"menuType\":\"H\",\"menuImg\":\"http://localhost:8080/profile/upload/2020/12/25/a6e063ce-aa3d-47b0-a649-aa6eea84e79e.jpg\",\"orderNum\":1,\"menuPrice\":15,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":15}]}},\"canteenId\":3,\"mealDateFont\":\"2021-02-01\",\"mealType\":\"zao\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-02-01 18:56:53');
INSERT INTO `sys_oper_log` VALUES (231, '新增食堂绑定的菜单', 1, 'com.wuhuacloud.web.controller.fb.CanteenController.editDayCanteenMenu()', 'POST', 1, 'admin', NULL, '/fb/canteen/day/menu', '116.23.97.237', 'XX XX', '{\"data\":{\"2021-02-01\":{\"zhone\":[{\"createBy\":\"\",\"createTime\":\"2021-01-23 13:52:25\",\"updateBy\":\"\",\"remark\":\"\",\"params\":{},\"menuId\":5,\"menuName\":\"大刀肉\",\"menuType\":\"H\",\"menuImg\":\"http://qn6her7ha.hn-bkt.clouddn.com/upload1/20210123/2d9740e5733f4af9b7909ef594461fbf.jfif\",\"orderNum\":0,\"menuPrice\":20,\"visible\":\"0\",\"status\":\"0\",\"canteenPrice\":20}]}},\"canteenId\":3,\"mealDateFont\":\"2021-02-01\",\"mealType\":\"zhone\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2021-02-01 18:57:43');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2020-12-21 17:51:27', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2020-12-21 17:51:27', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2020-12-21 17:51:27', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2020-12-21 17:51:27', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2020-12-21 17:51:27', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2020-12-21 17:51:27', 'admin', '2021-01-08 19:35:51', '普通角色');
INSERT INTO `sys_role` VALUES (3, '食堂管理员', 'canteen', 0, '1', 1, 1, '0', '0', 'admin', '2021-01-12 10:16:57', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 103);
INSERT INTO `sys_role_dept` VALUES (2, 104);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (3, 1061);
INSERT INTO `sys_role_menu` VALUES (3, 1062);
INSERT INTO `sys_role_menu` VALUES (3, 1063);
INSERT INTO `sys_role_menu` VALUES (3, 1064);
INSERT INTO `sys_role_menu` VALUES (3, 1065);
INSERT INTO `sys_role_menu` VALUES (3, 1067);
INSERT INTO `sys_role_menu` VALUES (3, 1068);
INSERT INTO `sys_role_menu` VALUES (3, 1069);
INSERT INTO `sys_role_menu` VALUES (3, 1070);
INSERT INTO `sys_role_menu` VALUES (3, 1071);
INSERT INTO `sys_role_menu` VALUES (3, 1072);
INSERT INTO `sys_role_menu` VALUES (3, 1073);
INSERT INTO `sys_role_menu` VALUES (3, 1074);
INSERT INTO `sys_role_menu` VALUES (3, 1075);
INSERT INTO `sys_role_menu` VALUES (3, 1076);
INSERT INTO `sys_role_menu` VALUES (3, 1077);
INSERT INTO `sys_role_menu` VALUES (3, 1078);
INSERT INTO `sys_role_menu` VALUES (3, 1079);
INSERT INTO `sys_role_menu` VALUES (3, 1086);
INSERT INTO `sys_role_menu` VALUES (3, 1087);
INSERT INTO `sys_role_menu` VALUES (3, 1091);
INSERT INTO `sys_role_menu` VALUES (3, 1092);
INSERT INTO `sys_role_menu` VALUES (3, 1093);
INSERT INTO `sys_role_menu` VALUES (3, 1094);
INSERT INTO `sys_role_menu` VALUES (3, 1095);
INSERT INTO `sys_role_menu` VALUES (3, 1096);
INSERT INTO `sys_role_menu` VALUES (3, 1097);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '超级管理员', '00', '', '', '0', '/profile/avatar/2020/12/25/d03fe7bd-6be4-4706-b2b9-918b17ca2f7c.jpeg', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2020-12-21 17:51:27', 'admin', '2020-12-21 17:51:27', 'admin', '2021-01-08 10:56:49', '管理员');
INSERT INTO `sys_user` VALUES (3, 103, 'wenfl', '温发良', '00', '', '', '0', '', '$2a$10$ITVVu.w3B4QeoJw6jGepBemXoC/QUbRIbudTMs8D.KCiCQXW579SC', '0', '0', '', NULL, 'admin', '2021-01-08 17:49:46', 'admin', '2021-01-12 10:12:30', NULL);
INSERT INTO `sys_user` VALUES (4, 103, 'jiangl', '蒋雷', '00', '', '', '2', '', '$2a$10$hKr2poFT37mimLu.goQFtuNqAGhXzv2SJxHPC8W0UBprtAgKAYsF.', '0', '0', '', NULL, 'admin', '2021-01-12 10:15:07', 'admin', '2021-01-12 10:23:49', NULL);
INSERT INTO `sys_user` VALUES (5, 103, 'wubt', '吴博涛', '00', '', '', '2', '', '$2a$10$MKO3Sea.JbaeMGfeg7HJzuwypLXMdL7K3i968cuPzLaFGhxCRIGde', '0', '0', '', NULL, 'admin', '2021-01-12 10:24:16', 'wubt', '2021-01-15 23:55:19', NULL);
INSERT INTO `sys_user` VALUES (6, 103, 'zhaoyq', '赵亚强', '00', '', '', '0', '', '$2a$10$vX6p2h7f.To.AcyQrlqk4.FH.n3/a446elS/8wUV6w0wbLTdwBYHy', '0', '0', '', NULL, 'admin', '2021-01-12 10:24:48', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (7, 103, 'chenyc', '陈仪椿', '00', '', '', '0', '', '$2a$10$dL7VcPEW4rD1.xrhgkc0zeAocby4M6x7Yq85I0GGjrlps1NBxZXoS', '0', '0', '', NULL, 'admin', '2021-01-12 10:51:15', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (900000, 103, 'dingw', '丁卫', '00', '', '', '0', '', '$2a$10$c5tihBbJE5I8zScy8P1rmeUItHPBqNj/cq5uNVd8KyMk4AUapMou.', '0', '0', '', NULL, 'admin', '2021-01-12 18:39:35', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (3, 2);
INSERT INTO `sys_user_role` VALUES (4, 2);
INSERT INTO `sys_user_role` VALUES (4, 3);

SET FOREIGN_KEY_CHECKS = 1;
