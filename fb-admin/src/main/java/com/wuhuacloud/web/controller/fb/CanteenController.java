package com.wuhuacloud.web.controller.fb;

import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.core.page.TableDataInfo;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.fb.domain.Canteen;
import com.wuhuacloud.fb.service.ICanteenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 食堂Controller
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/fb/canteen")
public class CanteenController extends BaseController
{
    @Autowired
    private ICanteenService canteenService;

    /**
     * 查询食堂列表
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:list')")
    @GetMapping("/list")
    public TableDataInfo list(Canteen canteen)
    {
        startPage();
        List<Canteen> list = canteenService.selectCanteenList(canteen);
        return getDataTable(list);
    }

    /**
     * 查询食堂列表所有
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:list')")
    @GetMapping("/list/all")
    public AjaxResult listAll()
    {
        List<Canteen> list = canteenService.selectCanteenList(new Canteen());
        return AjaxResult.success(list);
    }

    /**
     * 导出食堂列表
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:export')")
    @Log(title = "食堂", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Canteen canteen)
    {
        List<Canteen> list = canteenService.selectCanteenList(canteen);
        ExcelUtil<Canteen> util = new ExcelUtil<Canteen>(Canteen.class);
        return util.exportExcel(list, "canteen");
    }

    /**
     * 获取食堂详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(canteenService.selectCanteenById(id));
    }

    /**
     * 新增食堂
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:add')")
    @Log(title = "食堂", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Canteen canteen)
    {
        return toAjax(canteenService.insertCanteen(canteen));
    }

    /**
     * 修改食堂
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:edit')")
    @Log(title = "食堂", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Canteen canteen)
    {
        return toAjax(canteenService.updateCanteen(canteen));
    }

    /**
     * 删除食堂
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:remove')")
    @Log(title = "食堂", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(canteenService.deleteCanteenByIds(ids));
    }

    /**
     * 查询食堂绑定的菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:list')")
    @GetMapping("/day/menu")
    public AjaxResult getDayCanteenMenu(String mealDate,String mealType,Long canteenId)
    {
        return AjaxResult.success(canteenService.getDayCanteenMenu(mealDate,mealType,canteenId));
    }

    /**
     * 新增食堂绑定的菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:add')")
    @Log(title = "新增食堂绑定的菜单", businessType = BusinessType.INSERT)
    @PostMapping("/day/menu")
    public AjaxResult editDayCanteenMenu(@RequestBody Map dataMap)
    {
        canteenService.editDayCanteenMenu(dataMap);
        return AjaxResult.success();
    }

    /**
     * 删除食堂绑定的菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:remove')")
    @Log(title = "食堂菜单删除", businessType = BusinessType.DELETE)
    @PostMapping("/day/menu/del")
    public AjaxResult delCanteenMenut(@RequestBody Map dataMap)
    {
        canteenService.delCanteenMenut(dataMap);
        return AjaxResult.success();
    }

    /**
     * 查询食堂绑定的部门
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:list')")
    @GetMapping("/selectCanteenDept/{deptId}")
    public AjaxResult selectCanteenDept(@PathVariable Long deptId)
    {
        return AjaxResult.success(canteenService.selectCanteenDept(deptId));
    }

    /**
     * 新增食堂
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen:add')")
    @Log(title = "部门绑定食堂", businessType = BusinessType.INSERT)
    @PostMapping("/saveCanteenDept")
    public AjaxResult addCanteenDept(@RequestBody Map data)
    {
        Long deptId = Long.valueOf((int) data.get("deptId"));
        ArrayList<String> canteenIds = (ArrayList<String>) data.get("canteens");

        //删除已配置
        canteenService.delCanteenDeop(deptId);
        for(String canteenId : canteenIds){
            canteenService.saveCanteenDeop(deptId,canteenId);
        }
        return AjaxResult.success();
    }
}
