package com.wuhuacloud.web.controller.fb;

import java.util.List;

import com.wuhuacloud.common.core.domain.entity.SysDictData;
import com.wuhuacloud.common.utils.SecurityUtils;
import com.wuhuacloud.system.service.ISysDictTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.fb.domain.CanteenTime;
import com.wuhuacloud.fb.service.ICanteenTimeService;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.common.core.page.TableDataInfo;

/**
 * 食堂营业时间Controller
 * 
 * @author wenfl
 * @date 2021-01-06
 */
@RestController
@RequestMapping("/fb/canteen_time")
public class CanteenTimeController extends BaseController
{
    @Autowired
    private ICanteenTimeService canteenTimeService;

    @Autowired
    private ISysDictTypeService sysDictTypeService;

    /**
     * 查询食堂营业时间列表
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:list')")
    @GetMapping("/list")
    public TableDataInfo list(CanteenTime canteenTime)
    {
        startPage();
        List<CanteenTime> list = canteenTimeService.selectCanteenTimeList(canteenTime);
        return getDataTable(list);
    }

    /**
     * 导出食堂营业时间列表
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:export')")
    @Log(title = "食堂营业时间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CanteenTime canteenTime)
    {
        List<CanteenTime> list = canteenTimeService.selectCanteenTimeList(canteenTime);
        ExcelUtil<CanteenTime> util = new ExcelUtil<CanteenTime>(CanteenTime.class);
        return util.exportExcel(list, "canteen_time");
    }

    /**
     * 获取食堂营业时间详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:query')")
    @GetMapping(value = "/{canteenId}")
    public AjaxResult getInfo(@PathVariable("canteenId") Long canteenId)
    {
        CanteenTime canteenTime = new CanteenTime();
                    canteenTime.setCanteenId(canteenId);

        List<CanteenTime> canteenTimeList = canteenTimeService.selectCanteenTimeList(canteenTime);

        //如果没有值，默认初始化空的餐类型
        if(canteenTimeList.size()==0){
            List<SysDictData>  sysDictDataList =sysDictTypeService.selectDictDataByType("fb_meal_type");
            if(sysDictDataList.size()>0){
                for(SysDictData sysDictData :sysDictDataList){
                    canteenTime = new CanteenTime();
                    canteenTime.setOrderNum(sysDictData.getDictSort().intValue());
                    canteenTime.setCanteenId(canteenId);
                    canteenTime.setMealType(sysDictData.getDictValue());
                    canteenTimeList.add(canteenTime);
                }
            }
        }

        return AjaxResult.success(canteenTimeList);
    }

    /**
     * 新增食堂营业时间
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:add')")
    @Log(title = "食堂营业时间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<CanteenTime> canteenTimeList)
    {
        if(canteenTimeList.size()>0) {
            canteenTimeService.deleteCanteenTimeById(canteenTimeList.get(0).getCanteenId().toString());
            for (CanteenTime canteenTime : canteenTimeList) {
                canteenTime.setCreateBy(SecurityUtils.getUsername());
                canteenTimeService.insertCanteenTime(canteenTime);
            }
        }
        return AjaxResult.success();
    }

    /**
     * 修改食堂营业时间
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:edit')")
    @Log(title = "食堂营业时间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CanteenTime canteenTime)
    {
        return toAjax(canteenTimeService.updateCanteenTime(canteenTime));
    }

    /**
     * 删除食堂营业时间
     */
    @PreAuthorize("@ss.hasPermi('fb:canteen_time:remove')")
    @Log(title = "食堂营业时间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{createBys}")
    public AjaxResult remove(@PathVariable String[] canteenId)
    {
        return toAjax(canteenTimeService.deleteCanteenTimeByIds(canteenId));
    }
}
