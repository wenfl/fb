package com.wuhuacloud.web.controller.fb;

import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.core.page.TableDataInfo;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.common.utils.SecurityUtils;
import com.wuhuacloud.common.utils.StringUtils;
import com.wuhuacloud.common.utils.ip.IpUtils;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.fb.domain.TradeInfo;
import com.wuhuacloud.fb.domain.Wallet;
import com.wuhuacloud.fb.service.ITradeInfoService;
import com.wuhuacloud.fb.service.IWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 余额Controller
 * 
 * @author wenfl
 * @date 2021-01-04
 */
@RestController
@RequestMapping("/fb/wallet")
public class WalletController extends BaseController
{
    @Autowired
    private IWalletService walletService;

    @Autowired
    private ITradeInfoService tradeInfoService;

    /**
     * 查询余额列表
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:list')")
    @GetMapping("/list")
    public TableDataInfo list(Wallet wallet)
    {
        startPage();
        List<Wallet> list = walletService.selectWalletList(wallet);
        return getDataTable(list);
    }

    /**
     * 导出余额列表
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:export')")
    @Log(title = "余额", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Wallet wallet)
    {
        List<Wallet> list = walletService.selectWalletList(wallet);
        ExcelUtil<Wallet> util = new ExcelUtil<Wallet>(Wallet.class);
        return util.exportExcel(list, "wallet");
    }

    /**
     * 获取余额详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(walletService.selectWalletById(userId));
    }

    /**
     * 新增余额
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:add')")
    @Log(title = "余额", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Wallet wallet)
    {
        return toAjax(walletService.insertWallet(wallet));
    }

    /**
     * 修改余额
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:edit')")
    @Log(title = "余额", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Wallet wallet)
    {
        return toAjax(walletService.updateWallet(wallet));
    }

    /**
     * 删除余额
     */
    @PreAuthorize("@ss.hasPermi('fb:wallet:remove')")
    @Log(title = "余额", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(walletService.deleteWalletByIds(userIds));
    }


    /**
     * 充值金额
     * @param userIds
     * @param amount 金额
     * @return
     */
    @GetMapping(value = "/recharge")
    public AjaxResult recharge(Long[] userIds,BigDecimal amount){

        try{
            for(Long userId : userIds){
                Wallet wallet = walletService.selectWalletById(userId);
                if (StringUtils.isNull(wallet)){

                    wallet = new Wallet();
                    wallet.setUserId(userId);
                    wallet.setBalance(amount);
                    wallet.setCreateBy(SecurityUtils.getUsername());
                    walletService.insertWallet(wallet);
                }else{
                    wallet.setUserId(userId);
                    wallet.setBalance(wallet.getBalance().add(amount));
                    wallet.setUpdateBy(SecurityUtils.getUsername());
                    walletService.updateWallet(wallet);
                }

                TradeInfo tradeInfo = new TradeInfo();
                //类型
                tradeInfo.setTradeType("chong");
                //重置充值金额
                tradeInfo.setPayment("+"+amount.toString());
                //交易用户
                tradeInfo.setUserId(userId);
                //交易IP
                tradeInfo.setTradeIp(IpUtils.getHostIp());
                //
                tradeInfo.setCreateBy(SecurityUtils.getUsername());
                tradeInfoService.insertTradeInfo(tradeInfo);
            }
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.error("充值失败");
        }
        return AjaxResult.success("充值成功");
    }
}
