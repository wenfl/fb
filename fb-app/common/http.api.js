
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	// 此处使用了传入的params参数，一切自定义即可
	//获取token
	let login = (params = {}) => vm.$u.get('/login', params);
	
	//获取用户信息
	let getInfo = (params = {}) => vm.$u.get('/user', {});
	
	//获取餐类型
	let getMealType = (params = {}) => vm.$u.get('/canteen/'+params.canteenId+'/meal/type', {});
	
	//获取食堂列表信息
	let getCanteenList= (params = {}) => vm.$u.get('/canteen', {});
	
	//获取本周食堂菜单信息
	let getMenuWeekData= (params = {}) => vm.$u.get('/canteen/menu/'+params.canteenId, {});
	
	//获取账号余额信息
	let getBalance= (params = {}) => vm.$u.get('/wallet/balance', {});
	
	//修改密码
	let modifyPassword=  (params = {}) => vm.$u.post('/modifyPassword', params);
	
	//查询消费记录
	let getTradeInfo=  (params = {}) => vm.$u.get('/canteen/trade', params);
	
	//增加消费
	let addCanteenTrade= (params = {}) => vm.$u.post('/canteen/trade', params);
	
	//查询月总支出、收入、充值、补贴
	let getWalletInfomMonth=  (params = {}) => vm.$u.get('/canteen/trade/all', params);
	
	//查询通知公告类型
	let getNoticeType = (params = {}) => vm.$u.get('/notice/type', {});
	
	//获取通知公告列表
	let getNoticeList = (params = {}) => vm.$u.get('/notice', params);
	
	//获取意见列表
	let getComplainList = (params = {}) => vm.$u.get('/suggest', params);
	
	//新增意见
	let addComplain = (params = {}) => vm.$u.post('/suggest', params);
	
	//删除文件
	let delFile = (params = {}) => vm.$u.post('/upload/del', params);
	
	
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {login,getInfo,getMealType,getCanteenList,getBalance,getMenuWeekData,
				 modifyPassword,getTradeInfo,getWalletInfomMonth,getNoticeType,getNoticeList,
				 getComplainList,addComplain,delFile,addCanteenTrade};
}

export default {
	install
}