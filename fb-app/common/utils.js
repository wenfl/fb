// 数据字典转换
export function selectDictValue(datas, value) {
    var actions = [];
    Object.keys(datas).map((key) => {
        if (datas[key].text == (''+ value)) {
        actions.push(datas[key].value);
        return false;
        }
    })
    return actions.join('');
}


//加密方法
export function encryption(unEncrypted) {
    unEncrypted += '/Rick'; 
    let code = '';
    let key = 0x75;
    for (let i = 0; i < unEncrypted .length; i++) {
        code  += String.fromCharCode(unEncrypted .charCodeAt(i) ^ key);
    }
    return code;
}

//解密方法
export function decryption(encrypted) {
    let code = '';
    let key = 0x75;
    for (var i = 0; i < encrypted.length; i++) {
        code += (String.fromCharCode(encrypted.charCodeAt(i) ^ key));
    }
    code = code.replace(/\/Rick/,'')
    return code;
}