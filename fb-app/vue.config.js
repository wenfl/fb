module.exports = {
	// 配置路径别名
	configureWebpack: {
		devServer: {
			// 调试时允许内网穿透，让外网的人访问到本地调试的H5页面
			disableHostCheck: true,
			// port: 7878,
			// proxy: {
			//  // detail: https://cli.vuejs.org/config/#devserver-proxy http://192.168.2.163:8603/api
			// 	'/api': {
			// 		target: "http://192.168.4.50:8603",
			// 		changeOrigin: true,
			// 		pathRewrite: {'^/api': ''}
			// 	}
			// },
		}
	}
}
