## 平台简介
食堂管理系统项目包括：后台配置+微信小程序点餐，详情搭建启动脚本即可
小程序项目使用uniapp开发，可以打包成其他平台，UI采用uview-ui
修改config.js中server和serverUpload的url



**目前待完善消费端，消费端为扫描用户出示小程序中的二维码付款完成交易，消费端可以为固定设备，显示每个人当天当前时间点的消费记录，以食堂为单位

* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。

注：基础结构来源若依,文档（http://doc.ruoyi.vip/ruoyi-vue/document/hjbs.html）

目前后台功能模块
1.意见
    通过小程序反馈意见，后台查看并回复
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/145803_64b61522_1547270.png "屏幕截图.png")

2.食堂
    新增食堂，设置营业时间及添加食堂菜单
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/145901_c876b504_1547270.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/145949_eea18184_1547270.png "屏幕截图.png")

3.食堂菜单
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/150010_61c58681_1547270.png "屏幕截图.png")

4.交易记录
小程序中的消费，通过后台查看记录
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/150048_905d0305_1547270.png "屏幕截图.png")

5.余额
可给用户充值及查看充值记录
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/150116_4eb79cd6_1547270.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0524/150132_4aec44a2_1547270.png "屏幕截图.png")


## 小程序端演示图
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150237_3688bd95_1547270.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150307_f75bc582_1547270.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150341_c4b5f15f_1547270.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150414_5ef3d2af_1547270.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150453_d92350aa_1547270.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150525_e28f7c57_1547270.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/150551_26371c0a_1547270.png"/></td>
        <td></td>
    </tr>	 
</table>



